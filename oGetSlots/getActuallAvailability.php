<?php

#overlap

class getActuallAvailability{
  private $availability;
  private $mergedBookings;
  private $actuallAvailability;

  function __construct ($availability, $mergedBookings){
    $this->availability = $availability;
    $this->mergedBookings = $mergedBookings;
    $this->actuallAvailability = array();
    $this->getActuallAvailability();
  }
  function getActuallAvailability(){
    for($i = 0; $i < count($this->availability); $i++){
      $blocked = false;

      $as = new DateTime($this->availability[$i]['startTime']);
      $ae = new DateTime($this->availability[$i]['endTime']);

      for($b = 0; $b < count($this->mergedBookings); $b++){

        $bs = $this->mergedBookings[$b]['startTime'];
        $be = $this->mergedBookings[$b]['endTime'];

        if($as->format("H:i:s") >= $bs->format("H:i:s") && $ae->format("H:i:s") <= $be->format("H:i:s")){
          $block = "YES";
        } else if($as->format("H:i:s") < $bs->format("H:i:s") && $ae->format("H:i:s") >= $bs->format("H:i:s") && $ae->format("H:i:s") <= $be->format("H:i:s")){
          $block = "PARTIAL";

          array_push($this->actuallAvailability, array("startTime" => $as->format("G:i"), "endTime" => $bs->format("G:i")));  

        } else if($be->format("H:i:s") < $ae->format("H:i:s") && $as->format("H:i:s") >= $bs->format("H:i:s") && $as->format("H:i:s") <= $be->format("H:i:s")){
          $block = "PARTIAL";

          array_push($this->actuallAvailability, array("startTime" => $be->format("G:i"), "endTime" => $ae->format("G:i")));
        } else if($as->format("H:i:s") <= $bs->format("H:i:s") && $ae->format("H:i:s") >= $be->format("H:i:s")){
          $block = "PARTIAL";

          array_push($this->actuallAvailability, array("startTime" => $as->format("G:i"), "endTime" => $bs->format("G:i")));  
          array_push($this->actuallAvailability, array("startTime" => $be->format("G:i"), "endTime" => $ae->format("G:i")));  

        }
      }
      if(@!$blocked){
        array_push($this->actuallAvailability, array(
          "startTime" => $this->availability[$i]['startTime'],
          "endTime" => $this->availability[$i]['endTime']
        ));
      }
    }
  
    $finalActuallAvailibity = array();
    for($i = 0; $i < count($this->actuallAvailability); $i++){
      if(count($finalActuallAvailibity) > 0){
        for($f = 0; $f < count($finalActuallAvailibity); $f++){
          if(strtotime($finalActuallAvailibity[$f]['startTime']) < strtotime($this->actuallAvailability[$i]['startTime'])){
            array_push($finalActuallAvailibity, $this->actuallAvailability[$i]);
          } else {

          }
        }
      } else {
        array_push($finalActuallAvailibity, $this->actuallAvailability[$i]);
      }
    }

    $mergedFinalActuallAvailability = array();

    for($i = 0; $i < count($finalActuallAvailibity); $i++){
      if($i > 0){
        if($finalActuallAvailibity[$i]['startTime'] !== $finalActuallAvailibity[$i - 1]['startTime']){
          array_push($mergedFinalActuallAvailability, $finalActuallAvailibity[$i]);
        }
      } else {
        array_push($mergedFinalActuallAvailability, $finalActuallAvailibity[$i]);
      }
    }

    $this->actuallAvailability = $mergedFinalActuallAvailability;
  }

  function getData(){
    return $this->actuallAvailability;
  }
}

?>