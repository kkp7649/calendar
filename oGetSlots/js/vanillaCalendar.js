var slots;

var vanillaCalendar = {
  month: document.querySelectorAll('[data-calendar-area="month"]')[0],
  next: document.querySelectorAll('[data-calendar-toggle="next"]')[0],
  previous: document.querySelectorAll('[data-calendar-toggle="previous"]')[0],
  label: document.querySelectorAll('[data-calendar-label="month"]')[0],
  activeDates: null,
  date: new Date(),
  todaysDate: new Date(),

  init: function (options) {
    this.options = options
    this.date.setDate(1)
    this.createMonth()
    this.createListeners();
    $.ajax({
      url: 'getSlot.php',
      type: 'POST',
      data: {staffid: 1, userTimezone: Intl.DateTimeFormat().resolvedOptions().timeZone},
    })
    .done(function(data) {
      console.log("success");
      try {
        slots = JSON.parse(data);


        if(!slots.error){
        } else{
          var availableTimes = document.querySelector("#availableTimes");
          availableTimes.innerHTML = `
          <h6 class="mt-3" style="color: #de0000; font-size: 1.2rem;">
          ${slots.error}
          </h6>`;
        }
      } catch(e) {
        var availableTimes = document.querySelector("#availableTimes");
        availableTimes.innerHTML = `
        <h6 class="mt-3" style="color: #de0000; font-size: 1.2rem;">
        Internal Server Error!<br/> Cannot Fetch slot
        </h6>`;
      }
    })
    .fail(function() {
      console.log("error");
      var availableTimes = document.querySelector("#availableTimes");
      availableTimes.innerHTML = `
      <h6 class="mt-3" style="color: #de0000; font-size: 1.2rem;">
      Internal Server Error!<br/> Cannot Fetch slot
      </h6>`;
    });
  },

  createListeners: function () {
    var _this = this
    this.next.addEventListener('click', function () {
      _this.clearCalendar()
      var nextMonth = _this.date.getMonth() + 1
      _this.date.setMonth(nextMonth)
      _this.createMonth()
      var daysName = ['SUN', 'MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT'];
      if(_this.date.getMonth() > new Date().getMonth()){
        var supply = daysName[_this.date.getDay()]+" 01 "+_this.monthsAsString(_this.date.getMonth()).substring(0, 3)+" "+_this.date.getFullYear();
      } else{
        var supply = null;
      }
      document.getElementById("availableTimesHeading").innerHTML = "";
      document.getElementById("availableTimes").innerHTML = "";
      console.log(supply)
      $.ajax({
        url: 'getSlot.php',
        type: 'POST',
        data: {month: supply, staffid: 1, userTimezone: Intl.DateTimeFormat().resolvedOptions().timeZone},
      })
      .done(function(data) {
        console.log("success");
        try {
          slots = JSON.parse(data);


          if(!slots.error){
          } else{
            var availableTimes = document.querySelector("#availableTimes");
            availableTimes.innerHTML = `
            <h6 class="mt-3" style="color: #de0000; font-size: 1.2rem;">
            ${slots.error}
            </h6>`;
          }
        } catch(e) {
          var availableTimes = document.querySelector("#availableTimes");
          availableTimes.innerHTML = `
          <h6 class="mt-3" style="color: #de0000; font-size: 1.2rem;">
          Internal Server Error!<br/> Cannot Fetch slot
          </h6>`;
        }
      })
      .fail(function() {
        console.log("error");
        var availableTimes = document.querySelector("#availableTimes");
        availableTimes.innerHTML = `
        <h6 class="mt-3" style="color: #de0000; font-size: 1.2rem;">
        Internal Server Error!<br/> Cannot Fetch slot
        </h6>`;
      });
    })
    // Clears the calendar and shows the previous month
    this.previous.addEventListener('click', function () {
      _this.clearCalendar()
      var prevMonth = _this.date.getMonth() - 1
      _this.date.setMonth(prevMonth)
      document.getElementById("availableTimesHeading").innerHTML = "";
      document.getElementById("availableTimes").innerHTML = "";
      _this.createMonth()
      var daysName = ['SUN', 'MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT'];
      if(_this.date.getMonth() > new Date().getMonth()){
        var supply = daysName[_this.date.getDay()]+" 01 "+_this.monthsAsString(_this.date.getMonth()).substring(0, 3)+" "+_this.date.getFullYear();
      } else{
        var supply = null;
      }      console.log(supply)
      $.ajax({
        url: 'getSlot.php',
        type: 'POST',
        data: {month: supply, staffid: 1, userTimezone: Intl.DateTimeFormat().resolvedOptions().timeZone},
      })
      .done(function(data) {
        console.log("success");
        try {
          slots = JSON.parse(data);


          if(!slots.error){
          } else{
            var availableTimes = document.querySelector("#availableTimes");
            availableTimes.innerHTML = `
            <h6 class="mt-3" style="color: #de0000; font-size: 1.2rem;">
            ${slots.error}
            </h6>`;
          }
        } catch(e) {
          var availableTimes = document.querySelector("#availableTimes");
          availableTimes.innerHTML = `
          <h6 class="mt-3" style="color: #de0000; font-size: 1.2rem;">
          Internal Server Error!<br/> Cannot Fetch slot
          </h6>`;
        }
      })
      .fail(function() {
        console.log("error");
        var availableTimes = document.querySelector("#availableTimes");
        availableTimes.innerHTML = `
        <h6 class="mt-3" style="color: #de0000; font-size: 1.2rem;">
        Internal Server Error!<br/> Cannot Fetch slot
        </h6>`;
      });
    })
  },

  FetchSlot: async function(selectedDate){

    var date = new Date(selectedDate);
    await console.log(slots+"Fd")
    document.querySelector("#availableTimes").innerHTML = "";
    if(!slots[date.getDate()+"-"+(date.getMonth()+1)+"-"+date.getFullYear()] || slots[date.getDate()+"-"+(date.getMonth()+1)+"-"+date.getFullYear()].length < 1){
      var availableTimes = document.querySelector("#availableTimes");
      document.querySelector("#availableTimes").innerHTML = `
      <h6 class="mt-3" style="color: #de0000; font-size: 1.2rem;">
      No Slots Available!
      </h6>`;
    } else {
      var slot = slots[date.getDate()+"-"+(date.getMonth()+1)+"-"+date.getFullYear()];
      for(var i = 0; i < slot.length; i++){
        for(var timeSlots = 0; timeSlots < slot[i].length; timeSlots++){
          $("#availableTimes").append(`<a class="mt-3">${slot[i][timeSlots]}</a>`);
        }
      }
    }
    
  },

  createDay: function (num, day, year) {
    var newDay = document.createElement('div')
    var dateEl = document.createElement('span')
    dateEl.innerHTML = num
    newDay.className = 'vcal-date'
    newDay.setAttribute('data-calendar-date', this.date)

    // if it's the first day of the month
    if (num === 1) {
      if (day === 0) {
        newDay.style.marginLeft = (6 * 14.28) + '%'
      } else {
        newDay.style.marginLeft = ((day - 1) * 14.28) + '%'
      }
    }

    if (this.options.disablePastDays && this.date.getTime() <= this.todaysDate.getTime() - 1) {
      newDay.classList.add('vcal-date--disabled')
    } else {
      newDay.classList.add('vcal-date--active')
      newDay.setAttribute('data-calendar-status', 'active')
    }

    if (this.date.toString() === this.todaysDate.toString()) {
      newDay.classList.add('vcal-date--today')
    }

    newDay.appendChild(dateEl)
    this.month.appendChild(newDay)
  },

  dateClicked: function () {
    var _this = this
    this.activeDates = document.querySelectorAll(
      '[data-calendar-status="active"]'
      )
    for (var i = 0; i < this.activeDates.length; i++) {
      this.activeDates[i].addEventListener('click', function (event) {
        _this.removeActiveClass();
        document.querySelector("#availableTimesHeading").innerHTML = `
        <h6 style="font-size: 1.1rem; font-weight: 590; color: #222; margin-top: 1rem;">
        Showing slots on: 
        </h6>
        <h5 style="font-size: 1.5rem; color: #06a3ff;">
        ${new Date(this.dataset.calendarDate).getDate()}
        ${_this.monthsAsString(new Date(this.dataset.calendarDate).getMonth())}
        ${new Date(this.dataset.calendarDate).getFullYear()}
        </h5>`;
        this.classList.add('vcal-date--selected');

        _this.FetchSlot(this.dataset.calendarDate);
      })
    }
  },

  createMonth: function () {
    var currentMonth = this.date.getMonth()
    while (this.date.getMonth() === currentMonth) {
      this.createDay(
        this.date.getDate(),
        this.date.getDay(),
        this.date.getFullYear()
        )
      this.date.setDate(this.date.getDate() + 1)
    }
    // while loop trips over and day is at 30/31, bring it back
    this.date.setDate(1)
    this.date.setMonth(this.date.getMonth() - 1)

    this.label.innerHTML =
    this.monthsAsString(this.date.getMonth()) + ' ' + this.date.getFullYear()
    this.dateClicked()
  },

  monthsAsString: function (monthIndex) {
    return [
    'January',
    'Febuary',
    'March',
    'April',
    'May',
    'June',
    'July',
    'August',
    'September',
    'October',
    'November',
    'December'
    ][monthIndex]
  },

  clearCalendar: function () {
    vanillaCalendar.month.innerHTML = ''
  },

  removeActiveClass: function () {
    for (var i = 0; i < this.activeDates.length; i++) {
      this.activeDates[i].classList.remove('vcal-date--selected')
    }
  }
}