<?php

include 'mergeBlockArray.php';
include 'getActuallAvailability.php';

class getTimeSlot {
  private $date;
  private $availability;
  private $bookings;
  private $staffDetails;
  private $mergedBookings;

  private $finalArray;

  function __construct($date, $availability, $bookings, $staffDetails, $userTimezone){
    $this->date = $date;
    $this->availability = $availability;
    $this->bookings = $bookings;
    $this->staffDetails = $staffDetails;

    $this->userTimezone = new DateTimeZone($userTimezone);

    $this->actuallAvailability = array();
    $this->finalArray = array();

    $this->mergedBookings = new mergeBlockArray($this->staffDetails, $this->bookings);
    $this->mergedBookings = $this->mergedBookings->getMergedBookings();

    #Overlap

    $this->availability = new getActuallAvailability($this->availability, $this->mergedBookings);
    $this->availability = $this->availability->getData();

    $this->generateTimeSlots();

  }

  function generateTimeSlots(){
    for($i = 0; $i < count($this->availability); $i++){
      if($this->availability[$i]['startTime'] > $this->availability[$i]['endTime']){

        $startTime = new DateTime($this->date->format("Y-m-d ").$this->availability[$i]['startTime'], new DateTimeZone($this->staffDetails['details']['timezone']));
        $endTime = new DateTime($this->date->format("Y-m-d ").$this->availability[$i]['endTime'], new DateTimeZone($this->staffDetails['details']['timezone']));

        $todaysDateTime = new DateTime();

        $todaysDateTime->settimezone($this->userTimezone);
        $startTime->settimezone($this->userTimezone);
        $endTime->settimezone($this->userTimezone);

        $todaysDateTime->modify($this->staffDetails['details']['notice_period']);

        if(strtotime($todaysDateTime->format("Y-m-d")) <= strtotime($startTime->format("Y-m-d"))){

          while($startTime->format("H:i:s") < $endTime->format("H:i:s")){
            if($startTime->format("Y-m-d") == $todaysDateTime->format("Y-m-d")){
              $startTime = $todaysDateTime;
              $pushingEndTime = new DateTime($startTime->format("H:i:s"));
              $pushingEndTime->modify($this->staffDetails['duration']);
              array_push($this->finalArray, $startTime->format("H:i")." - ".$pushingEndTime->format("H:i"));
            } else {
              $pushingEndTime = new DateTime($startTime->format("H:i:s"));
              $pushingEndTime->modify($this->staffDetails['duration']);
              array_push($this->finalArray, $startTime->format("H:i")." - ".$pushingEndTime->format("H:i"));
            }
            $startTime->modify($this->staffDetails['details']['interval']."minutes");
          }
        }
      }
    }
  }

  function getData(){
    return array($this->finalArray);
  }

}

?>