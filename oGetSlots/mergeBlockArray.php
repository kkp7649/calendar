<?php

class mergeBlockArray {
  private $bookings;
  private $staffDetails;
  private $mergedBookings;

  function __construct ($staffDetails, $bookings){
    $this->bookings = $bookings;
    $this->staffDetails = $staffDetails;

    $this->mergedBookings = array();

    $this->mergeBlockArray();

  }
  function mergeBlockArray(){
    if(isset($this->bookings)){
      sort($this->bookings['slotBlock']['startTime']);
      sort($this->bookings['slotBlock']['endTime']);

      for($i = 0; $i < count($this->bookings['slotBlock']['startTime']); $i++){

        $startTime = $this->bookings['slotBlock']['startTime'][$i];
        $endTime = $this->bookings['slotBlock']['endTime'][$i];
        if(isset($this->bookings['slotBlock']['startTime'][$i+1]) && $this->bookings['slotBlock']['startTime'][$i]->format("H:i") == $this->bookings['slotBlock']['startTime'][$i+1]->format("H:i")){

          $endTime =  $this->bookings['slotBlock']['endTime'][$i+1];
          $i++;

        } else if(isset($this->bookings['slotBlock']['endTime'][$i+1]) && $this->bookings['slotBlock']['endTime'][$i]->format("H:i") == $this->bookings['slotBlock']['startTime'][$i+1]->format("H:i")){

          $endTime = $this->bookings['slotBlock']['endTime'][$i+1]; 
          $i++;
        } else if(isset($this->bookings['slotBlock']['endTime'][$i+1]) && isset( $this->mergedBookings[count($this->mergedBookings) - 1]) && $this->mergedBookings[count($this->mergedBookings) - 1]['startTime']->format("H:i") == $this->bookings['slotBlock']['endTime'][$i+1]->format("H:i")){
          $endTime = $this->bookings['slotBlock']['endTime'][$i+1]; 
          $i++;
        }

        $startTime = $startTime->sub(date_interval_create_from_date_string($this->staffDetails['bufferTime']));
        $endTime = $endTime->add(date_interval_create_from_date_string($this->staffDetails['bufferTime']));


        $startTime->settimezone(new DateTimeZone($this->staffDetails['details']['timezone']));
        $endTime->settimezone(new DateTimeZone($this->staffDetails['details']['timezone']));

        array_push($this->mergedBookings, array("startTime" => $startTime, "endTime" => $endTime));
      }
    } else {
      $this->bookings = array();
    }
  }

  function getMergedBookings(){
    return $this->mergedBookings;
  }
}

?>