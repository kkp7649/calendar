<?php 

class getSlots {
  private $startDate;  
  private $endDate;

  private $fetchWeekAvailibity;
  private $fetchDateOverrides;
  private $fetchStaffDetails;
  private $fetchBookingsQuery;

  private $connection;

  public $weeklyAvailbity;
  public $dateOverrides;
  public $staffDetails;

  private $bookings;

  private $month;

  private $gcalBookings;

  private $slots;

  function __construct(){
    $this->connection = @mysqli_connect("localhost", "root", "", "appointnow");

    if(!$this->connection){
      echo json_encode(array("error" => "Internal Server Error!<br/> Please Contact the administrator"));
      die();
    } else {
      #$this->staffId = $_POST['staffid'];
      $this->staffId = "1";

      if(isset($_POST['month'])){
        $this->month = $_POST['month'];
      }

      $this->slots = array();

      $this->userTimezone = "Asia/Calcutta";

      $this->getWeekAvailibity();
      $this->getStaffDetails();
      $this->getDateOverrides();
      $this->getBookings();
      $this->getTimeSlot();

      echo json_encode($this->slots);
    }
  }

  function getWeekAvailibity(){
    $this->fetchWeekAvailibity = "SELECT `monday`, `tuesday`, `wednesday`, `thursday`, `friday`, `saturday`, `sunday` FROM `staff_schedules` WHERE `staff_id` = ?";
    $stmt = @mysqli_stmt_init($this->connection);

    if(!@mysqli_stmt_prepare($stmt, $this->fetchWeekAvailibity)){
      echo json_encode(array(
        "error" => "Internal Server Error!<br/> Please Contact the administrator"
      ));
    } else {
      @mysqli_stmt_bind_param($stmt, "i", $this->staffId);
      @mysqli_stmt_execute($stmt);

      $result = @mysqli_stmt_get_result($stmt);
      $result = @mysqli_fetch_array($result, MYSQLI_ASSOC);

      $this->weeklyAvailbity = array(
        "monday" => $result['monday'], 
        "tuesday" => $result['tuesday'], 
        "wednesday" => $result['wednesday'], 
        "thursday" => $result['thursday'], 
        "friday" => $result['friday'], 
        "saturday" => $result['saturday'], 
        "sunday" => $result['sunday']
      );
    }
  }

  function getStaffDetails(){
    $this->fetchStaffDetails = "SELECT staffservices.buffer_time, staffservices.notice_period, staffservices.available_till, staffservices.meetings_count_perday, business_staff_members.timezone, staffservices.interval, staffservices.duration FROM `staffservices` INNER JOIN business_staff_members ON business_staff_members.id = ? WHERE staffservices.staff_id = ?";

    $stmt = @mysqli_stmt_init($this->connection);

    if(!@mysqli_stmt_prepare($stmt, $this->fetchStaffDetails)){
      echo json_encode(array(
        "error" => "Internal Server Error!<br/> Please Contact the administrator"
      ));
    } else {
      @mysqli_stmt_bind_param($stmt, "ii", $this->staffId, $this->staffId);
      @mysqli_stmt_execute($stmt);

      $result = @mysqli_stmt_get_result($stmt);
      $result = @mysqli_fetch_array($result, MYSQLI_ASSOC);
      $bufferTime = $result['buffer_time'];


      $bufferTime = explode(":", $bufferTime);

      $bufferTime = $bufferTime[0]." hours ".$bufferTime[1]." minutes";

      $duration = $result['duration'];

      $duration1 = explode(":", $duration);
      $duration = explode(":", $duration);

      $duration = $duration[0]." hours ".$duration[1]." minutes";

      $this->staffDetails = array("bufferTime" => $bufferTime, "details" => $result, "duration" => $duration, "durationHours" => $duration1[0], "durationMinutes" => $duration1[1]);
    }
  }

  function getDateOverrides(){
    $this->fetchDateOverrides = "SELECT `slots`, `date` FROM `date_overrides` WHERE `date` >= ? AND `date` <= ? AND `staff_id` = ?";

    $stmt = @mysqli_stmt_init($this->connection);

    if(!@mysqli_stmt_prepare($stmt, $this->fetchDateOverrides)){
      echo json_encode(array(
        "error" => "Internal Server Error!<br/> Please Contact the administrator"
      ));
    } else {
      if(isset($this->month)){
        $this->startDate = new DateTime($this->month);
      } else {
        $this->startDate = new DateTime("now");
      }
      $this->endDate = new DateTime("now");
      $this->endDate->modify("+".$this->staffDetails['details']['available_till']."days");

      $startDate = $this->startDate->format("Y-m-d");
      $endDate = $this->endDate->format("Y-m-d");

      @mysqli_stmt_bind_param($stmt, "ssi", $startDate, $endDate, $this->staffId);
      @mysqli_stmt_execute($stmt);

      $result = @mysqli_stmt_get_result($stmt);

      $this->dateOverrides = array();


      while($row = mysqli_fetch_array($result, MYSQLI_ASSOC)){
        $this->dateOverrides[$row['date']] = $row['slots'];
      }
    }
  }

  function getBookings(){
    $this->fetchBookingsQuery = "SELECT `slot`, `duration`, `date`, `buffer_time` FROM `bookings` WHERE `date` >= ? AND `date` <= ? AND `staff_id` = ? ORDER BY `slot`";

    $stmt = @mysqli_stmt_init($this->connection);

    if(!@mysqli_stmt_prepare($stmt, $this->fetchBookingsQuery)){
      echo json_encode(array(
        "error" => "Internal Server Error!<br/> Please Contact the administrator"
      ));
    } else {
      $startDate = $this->startDate->format("Y-m-d");
      $endDate = $this->endDate->format("Y-m-d");

      @mysqli_stmt_bind_param($stmt, "ssi", $startDate, $endDate, $this->staffId);
      @mysqli_stmt_execute($stmt);

      $result = @mysqli_stmt_get_result($stmt);

      $this->bookings = array();


      while($row = mysqli_fetch_array($result, MYSQLI_ASSOC)){
        $startTime = new DateTime($row['date'].$row['slot'], new DateTimeZone("GMT"));
        $endTime = new DateTime($row['date'].$row['slot'], new DateTimeZone("GMT"));
        $endTime->modify("+".$row['duration']);

        $bufferTime = $row['buffer_time'];

        $bufferTime = explode(":", $bufferTime);

        $startTime = $startTime->sub(date_interval_create_from_date_string($bufferTime[0]."hours ".$bufferTime[1]."minutes"));
        $endTime = $endTime->add(date_interval_create_from_date_string($bufferTime[0]."hours ".$bufferTime[1]."minutes"));
        if(isset($this->bookings[$row['date']])){
          array_push($this->bookings[$row['date']]['slotBlock']['startTime'], $startTime);
          array_push($this->bookings[$row['date']]['slotBlock']['endTime'], $endTime);

          $this->bookings[$row['date']]['noOfMettings'] = $this->bookings[$row['date']]['noOfMettings'] + 1;

        }else {
          $this->bookings[$row['date']] = array("slotBlock" => array(
            "startTime" => array($startTime), 
            "endTime" => array($endTime)), 
          "noOfMettings" => 1);
        }
      }
      $this->getGCalBookings();
    }
  }

  function getGCalBookings(){
    $this->gcalBookings = array(
      array(
        "startTime" => "2021-08-16T23:00:00Z",
        "endTime" => "2021-08-16T23:30:00Z"
      )
    );

    for($i = 0; $i < count($this->gcalBookings); $i++){
      $startTime = new DateTime($this->gcalBookings[$i]['startTime']);
      $endTime = new DateTime($this->gcalBookings[$i]['endTime']);

      $startTime = new DateTime($startTime->format("Y-m-dH:i:s"), new DateTimeZone("GMT"));
      $endTime = new DateTime($endTime->format("Y-m-dH:i:s"), new DateTimeZone("GMT"));

      if(isset($this->bookings[$startTime->format("Y-m-d")])){
        array_push($this->bookings[$startTime->format("Y-m-d")]['slotBlock']['startTime'], $startTime);
        array_push($this->bookings[$startTime->format("Y-m-d")]['slotBlock']['endTime'], $endTime);

        $this->bookings[$startTime->format("Y-m-d")]['noOfMettings'] = $this->bookings[$startTime->format("Y-m-d")]['noOfMettings'] + 1;
      } else {
        $this->bookings[$row['date']] = array("slotBlock" => array(
          "startTime" => array($startTime), 
          "endTime" => array($endTime)), 
        "noOfMettings" => 1);
      }
    }
  }

  function getTimeSlot(){
    while($this->startDate->format("Ymd") <= $this->endDate->format("Ymd")){
      $timeSlots = new getTimeSlot($this->startDate, $this->getAvailablity($this->startDate), @$this->bookings[$this->startDate->format("Y-m-d")], $this->staffDetails, $this->userTimezone);
      $this->slots[$this->startDate->format("j-n-Y")] = $timeSlots->getData();
      $this->startDate->modify("+1days");
    }
  }

  function getAvailablity($date){
    $availability = array();
    if(isset($this->dateOverrides[$date->format("Y-m-d")])){
      $dateOverride = $dateOverrides[$date->format("Y-m-d")];
      for($i = 0; $i < count($dateOverrides); $i++){
        array_push($availability, $dateOverrides[$i]);
      }
    } else {
      if(isset($this->weeklyAvailbity[strtolower($date->format("l"))])){
        $weeklyAvailbity = json_decode($this->weeklyAvailbity[strtolower($date->format("l"))]);
        for($i = 0; $i < count($weeklyAvailbity->start_time); $i++){
          array_push($availability, array(
            "startTime" => $weeklyAvailbity->start_time[$i], 
            "endTime" => $weeklyAvailbity->end_time[$i]
          ));
        }
      }
    }
    return $availability;
  }
}


include 'getTimeSlot.php';
$getSlot = new getSlots();



?>