<?php


function getDateOverrides($startDate, $endDate, $staff_id){
    include 'config.php';
    $sql = "SELECT `slots`, `date` FROM `date_overrides` WHERE `date` >= '$startDate' AND date <= '$endDate' AND `staff_id` = '$staff_id'";


    $stmt = mysqli_stmt_init($connection);

    if(!@mysqli_stmt_prepare($stmt, $sql)){
        if(!$production){
            echo json_encode(array("error" => @mysqli_stmt_error($stmt)));
        } else {
            echo json_encode(array("error" => "Internal Server Error! Please contact the administrator."));
        }
    } else {
        mysqli_stmt_execute($stmt);
        
        $result = mysqli_stmt_get_result($stmt);

        $data = array();
        while($row = mysqli_fetch_array($result, MYSQLI_ASSOC)){
            $data[$row['date']] = json_decode($row['slots']);
        }
    }
    return $data;
}

?>