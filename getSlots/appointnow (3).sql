-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 04, 2021 at 09:27 AM
-- Server version: 10.4.18-MariaDB
-- PHP Version: 8.0.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `appointnow`
--

-- --------------------------------------------------------

--
-- Table structure for table `bookings`
--

CREATE TABLE `bookings` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `staff_serviceid` int(11) NOT NULL,
  `staff_id` int(11) NOT NULL,
  `status` enum('Cancelled','Rescheduled','Active') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Active',
  `slot` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `duration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meeting_location` enum('zoom','googlemeet','custom') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'custom',
  `meeting_link` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `currency` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'USD',
  `date` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `amount` decimal(9,3) NOT NULL DEFAULT 0.000,
  `meeting_notes` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `cancelres_notes` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `meeting_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'onetoonemeet',
  `google_eventID` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `current_participants` int(11) NOT NULL DEFAULT 0,
  `max_participants` int(11) NOT NULL DEFAULT 0,
  `booking_ids` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `business_staff_members`
--

CREATE TABLE `business_staff_members` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('active','disabled') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'active',
  `zoom_active` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `zoom_refresh` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `paypal_token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `stripe_token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `timezone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'UTC',
  `language` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'English',
  `business_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `profile` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `logo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `g_selected_email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `g_calendar_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `g_calendars`
--

CREATE TABLE `g_calendars` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `staff_id` bigint(20) UNSIGNED NOT NULL,
  `access_token` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `refresh_token` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `calendar_block` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `account` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `staffservices`
--

CREATE TABLE `staffservices` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `status` enum('active','disabled','draft') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'draft',
  `staff_id` bigint(20) UNSIGNED NOT NULL,
  `paidappointment` tinyint(4) NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `price` decimal(8,2) NOT NULL DEFAULT 0.00,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `interval` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `duration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `schedule_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meeting_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'onetoonemeet',
  `participants_count` int(11) NOT NULL DEFAULT 0,
  `meetings_count_perday` int(11) NOT NULL DEFAULT 0,
  `notice_period` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `available_till` int(11) NOT NULL DEFAULT 0,
  `location` enum('zoom','meet','custom') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'custom',
  `paymode` enum('Offline','Stripe','Paypal') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Offline',
  `buffer_time` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `redirectURL` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cancelresc` tinyint(4) NOT NULL,
  `confirm_subject` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `confirm_body` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `cancel_subject` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `cancel_body` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `reschedule_subject` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `reschedule_body` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `followup_subject` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `followup_body` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `reminder_subject` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `reminder_body` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `reminder_timing` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `followup_timing` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `location_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `flag_bookingform` tinyint(4) NOT NULL DEFAULT 0,
  `reminder` tinyint(4) NOT NULL DEFAULT 0,
  `followup` tinyint(4) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `staff_schedules`
--

CREATE TABLE `staff_schedules` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `staff_id` bigint(20) UNSIGNED NOT NULL,
  `is_default` enum('yes','no') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'no',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `monday` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tuesday` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `wednesday` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `thursday` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `friday` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `saturday` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sunday` text COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bookings`
--
ALTER TABLE `bookings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `business_staff_members`
--
ALTER TABLE `business_staff_members`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `business_staff_members_code_unique` (`code`),
  ADD UNIQUE KEY `business_staff_members_g_selected_email_unique` (`g_selected_email`),
  ADD UNIQUE KEY `business_staff_members_g_calendar_id_unique` (`g_calendar_id`),
  ADD KEY `business_staff_members_business_id_foreign` (`business_id`);

--
-- Indexes for table `g_calendars`
--
ALTER TABLE `g_calendars`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `g_calendars_account_unique` (`account`),
  ADD KEY `g_calendars_staff_id_foreign` (`staff_id`);

--
-- Indexes for table `staffservices`
--
ALTER TABLE `staffservices`
  ADD PRIMARY KEY (`id`),
  ADD KEY `staffservices_staff_id_foreign` (`staff_id`);

--
-- Indexes for table `staff_schedules`
--
ALTER TABLE `staff_schedules`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `staff_schedules_name_staff_id_unique` (`name`,`staff_id`),
  ADD KEY `staff_schedules_staff_id_foreign` (`staff_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bookings`
--
ALTER TABLE `bookings`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `business_staff_members`
--
ALTER TABLE `business_staff_members`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `g_calendars`
--
ALTER TABLE `g_calendars`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `staffservices`
--
ALTER TABLE `staffservices`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `staff_schedules`
--
ALTER TABLE `staff_schedules`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `business_staff_members`
--
ALTER TABLE `business_staff_members`
  ADD CONSTRAINT `business_staff_members_business_id_foreign` FOREIGN KEY (`business_id`) REFERENCES `businesses` (`id`);

--
-- Constraints for table `g_calendars`
--
ALTER TABLE `g_calendars`
  ADD CONSTRAINT `g_calendars_staff_id_foreign` FOREIGN KEY (`staff_id`) REFERENCES `business_staff_members` (`id`);

--
-- Constraints for table `staffservices`
--
ALTER TABLE `staffservices`
  ADD CONSTRAINT `staffservices_staff_id_foreign` FOREIGN KEY (`staff_id`) REFERENCES `business_staff_members` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `staff_schedules`
--
ALTER TABLE `staff_schedules`
  ADD CONSTRAINT `staff_schedules_staff_id_foreign` FOREIGN KEY (`staff_id`) REFERENCES `business_staff_members` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
