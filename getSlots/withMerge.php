<?php

include 'config.php';
include 'getBookingsHardCoded.php';
include 'getDateOverrides.php';

include 'getStaffDetails.php';


$query = "SELECT staff_schedules.sunday, staff_schedules.monday, staff_schedules.tuesday, staff_schedules.wednesday, staff_schedules.thursday, staff_schedules.friday, staff_schedules.saturday FROM `staff_schedules` WHERE staff_schedules.staff_id = ?";

$stmt = @mysqli_stmt_init($connection);

if(!@mysqli_stmt_prepare($stmt, $query)){
    if(!$production){
        echo json_encode(array("error" => @mysqli_stmt_error($stmt)));
    } else {
        echo json_encode(array("error" => "Internal Server Error! Please contact the administrator."));
    }
} else {

    $staff_id = @mysqli_real_escape_string($connection, $_POST['staffid']);
    @mysqli_stmt_bind_param($stmt, "i", $staff_id);
    mysqli_stmt_execute($stmt);
    
    $result = mysqli_stmt_get_result($stmt);

    $weekAvailibities = array(
        "sunday" => new stdClass
    );

    $weekAvailibities['sunday']->start_time = array("12:00");
    $weekAvailibities['sunday']->end_time = array("17:00");

    $dateOverrides = array(
        '2021-08-15' => array(
            new stdClass
        )
    );

    $dateOverrides['2021-08-15'][0]->startTime = "12:00"; 
    $dateOverrides['2021-08-15'][0]->endTime = "14:00"; 

    // while($row = mysqli_fetch_array($result, MYSQLI_ASSOC)){

    //     $weekAvailibities['sunday'] = json_decode($row['sunday']);
    //     $weekAvailibities['monday'] = json_decode($row['monday']);
    //     $weekAvailibities['tuesday'] = json_decode($row['tuesday']); 
    //     $weekAvailibities['wednesday'] = json_decode($row['wednesday']); 
    //     $weekAvailibities['thursday'] = json_decode($row['thursday']);
    //     $weekAvailibities['friday'] = json_decode($row['friday']); 
    //     $weekAvailibities['saturday'] = json_decode($row['saturday']);

    // }

    $staffDetails = getStaffDetails($staff_id);

    // $timezone = new DateTimeZone($staffDetails['details']['timezone']);
    $timezone = new DateTimeZone("Asia/Calcutta");

    if(isset($_POST['month'])){
        $todaysDate = new DateTime($_POST['month'], $timezone);
        $endDate = new DateTime($_POST['month'], $timezone);
    } else {
        $todaysDate = new DateTime("now", $timezone);
        $endDate = new DateTime("now", $timezone);
    }

    $slots = array();
    // $endDate->modify("+".$staffDetails['details']['available_till']."day");
    $endDate->modify("+10day");
    $bookings = getBookings($todaysDate->format('Y-m-d'), $endDate->format('Y-m-d'), $staff_id, $staffDetails['bufferTime']);

    
    // $dateOverrides = getDateOverrides($todaysDate->format('Y-m-d'), $endDate->format('Y-m-d'), $staff_id);

    if(isset($_POST['month'])){
        $iterationDate = new DateTime($_POST['month'], $timezone);
    } else {
        $iterationDate = new DateTime("now", $timezone);
    }


    $nextMonth = $todaysDate->format("m") + 1;

    if(isset($_POST['month'])){

        $endDate = new DateTime($_POST['month'], $timezone);

    } else {
        $endDate = new DateTime("now", $timezone);
    }


    $endAvailable = new DateTime("now", $timezone);
    // $endAvailable->modify("+".$staffDetails['details']['available_till']."day");
    $endAvailable->modify("+10day");


    $running = true;
        
    while($iterationDate->format("m") < $nextMonth && $running){
        if($iterationDate->format("m") == $endAvailable->format("m")){
            if($iterationDate->format("d") == $endAvailable->format("d")){
                $running = false;
            }
        } else if($iterationDate->format("m") > $endAvailable->format("m")){
            $running = false;
        }

        if($running){

            // $slots[$iterationDate->format("j-n-Y")] = getSlot($iterationDate, $dateOverrides, $weekAvailibities, $staffDetails['details']['interval'], $staffDetails['duration'], new DateTimeZone($_POST['userTimezone']), $timezone, $bookings, $staffDetails['details']['meetings_count_perday'], $staffDetails['details']['notice_period']);
            $slots[$iterationDate->format("j-n-Y")] = getSlot($iterationDate, $dateOverrides, $weekAvailibities, 10,  30, new DateTimeZone($_POST['userTimezone']), $timezone, $bookings, 40, "10 mins");
            $iterationDate->modify("+1days");

        }
    }

    echo json_encode($slots);
}


function getSlot($currentDate, $dateOverrides, $weekAvailibities, $interval, $duration, $userTimezone, $timezone, $bookings, $meetings_count_perday, $notice_period){

    $startDay = $currentDate->format("d");
    $startMonth = $currentDate->format("m");
    $startDayTexual = strtolower($currentDate->format("l"));
    $startYear = $currentDate->format("Y");

    $slot = array($startYear."-".$startMonth."-".$startDay => array());

    $availableTimeArray = array("startTime" => array(), "endTime" => array());

    if(isset($dateOverrides[$startYear."-".$startMonth."-".$startDay])){
        $slots = $dateOverrides[$startYear."-".$startMonth."-".$startDay];
        for($i = 0; $i < count($slots); $i++){
            $as=new DateTime($startYear."-".$startMonth."-".$startDay.$slots[$i]->startTime,$timezone );
            $ae=new DateTime($startYear."-".$startMonth."-".$startDay.$slots[$i]->endTime,$timezone );

            $currentDateTime = new DateTime();
            
            $as->settimezone($userTimezone);
            $currentDateTime->settimezone($userTimezone);
            $ae->settimezone($userTimezone); 

            if($currentDateTime->format("d-m-Y") == $as->format("d-m-Y")){
                $as = $currentDateTime->add(date_interval_create_from_date_string($notice_period));                
            }

            if($as->format("H:i:s") < $ae->format("H:i:s")){
                if(isset($bookings[$as->format("Y-m-d")])){
                    $bookingsOFTHISDAY = $bookings[$as->format("Y-m-d")]['slotBlock'];
                    $block = null;
                    if($meetings_count_perday <= $bookings[$as->format("Y-m-d")]['noOfMettings']){
                        $block = "FULL";
                    }
                    
                    include "overlap.php";


                    if(!isset($block)){
                    array_push($availableTimeArray['startTime'], $as);
                    array_push($availableTimeArray['endTime'], $ae);
                    }
                }else{
                    array_push($availableTimeArray['startTime'], $as);
                    array_push($availableTimeArray['endTime'], $ae);
                }
            }

        }


    } else if(isset($weekAvailibities[$startDayTexual])){
        for($s = 0; $s < count($weekAvailibities[$startDayTexual]->start_time); $s++){
            $as=new DateTime($startYear."-".$startMonth."-".$startDay.$weekAvailibities[$startDayTexual]->start_time[$s],$timezone );
            $ae=new DateTime($startYear."-".$startMonth."-".$startDay." ".$weekAvailibities[$startDayTexual]->end_time[$s],$timezone );

            $as->settimezone($userTimezone);
            $ae->settimezone($userTimezone);


            if(isset($bookings[$as->format("Y-m-d")])){
                $bookingsOFTHISDAY = $bookings[$as->format("Y-m-d")]['slotBlock'];
                $block = null;
                for($i = 0; $i < count($bookingsOFTHISDAY); $i++){
                    $bs = $bookingsOFTHISDAY[$i]['startTime'];
                    $be = $bookingsOFTHISDAY[$i]['endTime'];

                    $bs->settimezone($userTimezone);
                    $be->settimezone($userTimezone);

                    if($meetings_count_perday <= $bookings[$as->format("Y-m-d")]['noOfMettings']){
                        $block = "FULL";
                    }
                    include "overlap.php";
                }

                if(!isset($block)){
                    array_push($availableTimeArray['startTime'], $as);
                    array_push($availableTimeArray['endTime'], $ae);
                }
            }else{
                array_push($availableTimeArray['startTime'], $as);
                    array_push($availableTimeArray['endTime'], $ae);
            }

            
        }
    }
    $availableTimeArray['startTime'] = array_unique($availableTimeArray['startTime']);
    $availableTimeArray['startTime'] = array_unique($availableTimeArray['endTime']);

    sort($availableTimeArray['startTime']);
    sort($availableTimeArray['endTime']);

    $finalAvailableTimeArray = array("startTime" => array(), "endTime" => array());

    // for($i = 0; $i < count($availableTimeArray['startTime']); $i++){
    //     if(isset($availableTimeArray['endTime'][$i+1]) && isset($availableTimeArray['startTime'][$i+1]) && $availableTimeArray['startTime'][$i] == $availableTimeArray['startTime'][$i+1]){
    //         if(strtotime($availableTimeArray['endTime'][$i] )< strtotime($availableTimeArray['endTime'][$i+1])){
    //             array_push($finalAvailableTimeArray['startTime'], $availableTimeArray['startTime'][$i]);
    //             array_push($finalAvailableTimeArray['endTime'], $availableTimeArray['endTime'][$i+1]);
    //         } else {
    //             array_push($finalAvailableTimeArray['startTime'], $availableTimeArray['startTime'][$i]);
    //             array_push($finalAvailableTimeArray['endTime'], $availableTimeArray['endTime'][$i]);
    //         }
    //     } else {
    //         array_push($finalAvailableTimeArray['startTime'], $availableTimeArray['startTime'][$i]);
    //             array_push($finalAvailableTimeArray['endTime'], $availableTimeArray['endTime'][$i]);
    //     };
    // }
    // 

    for($i = 0; $i < count($availableTimeArray['startTime']); $i++){
        $availableTimeArray['startTime'][$i] = new DateTime($availableTimeArray['startTime'][$i], $userTimezone);
        $availableTimeArray['endTime'][$i] = new DateTime($availableTimeArray['endTime'][$i], $userTimezone);
        if(isset($slot[$startYear."-".$startMonth."-".$startDay][0])){
            array_push($slot[$startYear."-".$startMonth."-".$startDay], getTimeSlot($availableTimeArray['startTime'][$i], $availableTimeArray['endTime'][$i], $interval, $duration, $userTimezone));
        } else {
            array_push($slot[$startYear."-".$startMonth."-".$startDay], getTimeSlot($availableTimeArray['startTime'][$i], $availableTimeArray['endTime'][$i], $interval, $duration, $userTimezone));
        }
    }
    return $slot[$startYear."-".$startMonth."-".$startDay];
}

function getTimeSlot($as, $ae, $interval, $duration, $userTimezone){
    $slots = array();

    $startTime = new DateTime($as->format("D M d Y H:i:s"), $userTimezone);
    $endTime = new DateTime($as->format("D M d Y H:i:s"), $userTimezone);

    if(strtotime($ae->format("H:i:s")) > strtotime($as->format("H:i:s"))){
        $endTime->modify("+".$duration."minutes");

        while(strtotime($startTime->format('H:i:s')) < strtotime($ae->format('H:i:s')) && $endTime->format("H:i:s") <= $ae->format("H:i:s")){

            array_push($slots, $startTime->format("H:i")." - ".$endTime->format("H:i"));
            $endTime->modify("+".$interval."minutes");
            $startTime->modify("+".$interval."minutes");

        }
    }

    return $slots;
}


?>