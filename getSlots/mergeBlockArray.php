<?php

$gcalData = json_encode($gcalData);
$gcalData = json_decode($gcalData);

$finalArray = array();

foreach ($data as $k=>$v) {
 for($i = 0; $i < count($v['slotBlock']); $i++){
    if(isset($v['slotBlock'][$i+1]['startTime']) && $v['slotBlock'][$i]['endTime']->format("H:i:s") == $v['slotBlock'][$i+1]['startTime']->format("H:i:s")){
        if(isset($finalArray[$k])){

            array_push($finalArray[$k]['slotBlock'], array("startTime" => $v['slotBlock'][$i]['startTime']->sub(date_interval_create_from_date_string($staffBufferTime)), "endTime" => $v['slotBlock'][$i+1]['endTime']->add(date_interval_create_from_date_string($staffBufferTime))));
        } else {
            $finalArray[$k] = array("slotBlock" => array(array("startTime" => $v['slotBlock'][$i]['startTime']->sub(date_interval_create_from_date_string($staffBufferTime)), "endTime" => $v['slotBlock'][$i+1]['endTime']->add(date_interval_create_from_date_string($staffBufferTime)))), "noOfMettings" => $v['noOfMettings']);
        }
        $i = $i+1;
    } else {
        if(isset($finalArray[$k])){
            array_push($finalArray[$k]['slotBlock'], array("startTime" => $v['slotBlock'][$i]['startTime']->sub(date_interval_create_from_date_string($staffBufferTime)), "endTime" => $v['slotBlock'][$i]['endTime']->add(date_interval_create_from_date_string($staffBufferTime))));
        } else {
            $finalArray[$k] = array("slotBlock" => array(array("startTime" => $v['slotBlock'][$i]['startTime']->sub(date_interval_create_from_date_string($staffBufferTime)), "endTime" => $v['slotBlock'][$i]['endTime']->add(date_interval_create_from_date_string($staffBufferTime)))), "noOfMettings" => $v['noOfMettings']);
        }
    };
}
}

for($i = 0; $i < count($gcalData); $i++){
    $startTime = new DateTime($gcalData[$i]->startTime);
    $endTime = new DateTime($gcalData[$i]->endTime);

    if(isset($finalArray[$startTime->format("Y-m-d")])){
        array_push($finalArray[$startTime->format("Y-m-d")]['slotBlock'], array("startTime" => $startTime, "endTime" => $endTime));
        $finalArray[$startTime->format("Y-m-d")]['noOfMettings'] = $finalArray[$startTime->format("Y-m-d")]['noOfMettings']+1;
    } else {
        $finalArray[$startTime->format("Y-m-d")] = array("slotBlock" => array(array("startTime" => $startTime->sub(date_interval_create_from_date_string($staffBufferTime)), "endTime" => $endTime->add(date_interval_create_from_date_string($staffBufferTime)))), "noOfMettings" => 1);
    }
}

return $finalArray;

?>