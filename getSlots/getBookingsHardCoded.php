<?php

function getBookings(){
    $staffBufferTime = "0 hours 0minutes";
    $data = array(
        '2021-08-15' => array(
            'slotBlock' => array(
                array(
                    "startTime" => new DateTime("2021-08-15 06:00:00", new DateTimeZone("GMT +5:30")),
                    "endTime" => new DateTime("2021-08-15 06:30:00", new DateTimeZone("GMT +5:30"))
                ),
                array(
                    "startTime" => new DateTime("2021-08-15 07:10:00", new DateTimeZone("GMT +5:30")),
                    "endTime" => new DateTime("2021-08-15 07:30:00", new DateTimeZone("GMT +5:30"))
                )
            ),
            "noOfMettings" => 1
        )
    );  

    $gcalData = array(
    );
    return include "mergeBlockArray.php";
}

?>