<?php

function getStaffDetails($staff_id){
    include 'config.php';
    $sql = "SELECT staffservices.buffer_time, staffservices.notice_period, staffservices.available_till, staffservices.meetings_count_perday, business_staff_members.timezone, staffservices.interval, staffservices.duration FROM `staffservices` INNER JOIN business_staff_members ON business_staff_members.id = '$staff_id' WHERE staffservices.staff_id = '$staff_id'";


    $stmt = mysqli_stmt_init($connection);

    if(!@mysqli_stmt_prepare($stmt, $sql)){
        if(!$production){
            echo json_encode(array("error" => @mysqli_stmt_error($stmt)));
        } else {
            echo json_encode(array("error" => "Internal Server Error! Please contact the administrator."));
        }
    } else {
        mysqli_stmt_execute($stmt);
        
        $result = mysqli_fetch_array(mysqli_stmt_get_result($stmt), MYSQLI_ASSOC);

        $bufferTime = $result['buffer_time'];

        $bufferTime = explode(":", $bufferTime);

        $bufferTime = $bufferTime[0]." hours ".$bufferTime[1]." minutes";

        $duration = $result['duration'];

        $duration = explode(":", $duration);

        $duration = $duration[0]." hours ".$duration[1]." minutes";

        return array("bufferTime" => $bufferTime, "details" => $result, "duration" => $duration);
    }  
}

?>