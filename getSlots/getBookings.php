<?php

function getBookings($startDate, $endDate, $staffId, $staffBufferTime){
    include 'config.php';
    $sql = "SELECT `slot`, `date`, `buffer_time`, `duration` FROM `bookings` WHERE `date` >= '$startDate' AND date <= '$endDate' AND `staff_id` = '$staffId' ORDER BY `slot`";

    $stmt = mysqli_stmt_init($connection);

    if(!@mysqli_stmt_prepare($stmt, $sql)){
        if(!$production){
            echo json_encode(array("error" => @mysqli_stmt_error($stmt)));
        } else {
            echo json_encode(array("error" => "Internal Server Error! Please contact the administrator."));
        }
    } else {
        mysqli_stmt_execute($stmt);
        
        $result = mysqli_stmt_get_result($stmt);

        $data = array();
        while($row = mysqli_fetch_array($result, MYSQLI_ASSOC)){

            $startTime = new DateTime($row['slot'], new DateTimeZone("GMT +00"));

            $buffer_time_array = explode(":", $row['buffer_time']);

            $startTime->sub(new DateInterval("PT".$buffer_time_array[0]."H"));
            $startTime->sub(date_interval_create_from_date_string($buffer_time_array[1]." minutes"));

            $endTime = new DateTime($row['slot'], new DateTimeZone("GMT +00"));

            $endTime->sub(new DateInterval("PT".$buffer_time_array[0]."H"));
            $endTime->sub(date_interval_create_from_date_string($buffer_time_array[1]." minutes"));

            $endTime->modify($row['duration']);

            if(!isset($data[$row['date']])){
                $data[$row['date']] = array('slotBlock' => array(array('startTime' => $startTime, 'endTime' => $endTime)), 'noOfMettings' => 1);
            } else {
                array_push($data[$row['date']]['slotBlock'], array('startTime' => $startTime, 'endTime' => $endTime));
                $data[$row['date']]['noOfMettings'] = $data[$row['date']]['noOfMettings']+1;
            }


        }

        mysqli_stmt_close($stmt);

        return include "mergeBlockArray.php";


    }
}

?>