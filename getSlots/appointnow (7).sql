-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 14, 2021 at 02:10 AM
-- Server version: 10.4.18-MariaDB
-- PHP Version: 8.0.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `appointnow`
--

-- --------------------------------------------------------

--
-- Table structure for table `accountsetting_staffs`
--

CREATE TABLE `accountsetting_staffs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `language` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `currentcy` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `time_formate` bigint(20) NOT NULL,
  `time_zone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `staff_id` bigint(20) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `activity_log`
--

CREATE TABLE `activity_log` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `log_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `subject_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subject_id` bigint(20) UNSIGNED DEFAULT NULL,
  `causer_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `causer_id` bigint(20) UNSIGNED DEFAULT NULL,
  `properties` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL CHECK (json_valid(`properties`)),
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `activity_log`
--

INSERT INTO `activity_log` (`id`, `log_name`, `description`, `subject_type`, `subject_id`, `causer_type`, `causer_id`, `properties`, `created_at`, `updated_at`) VALUES
(1, 'default', 'created', 'App\\Models\\User', 1, NULL, NULL, '{\"attributes\":{\"id\":1,\"name\":\"Guillermo Rolfson\",\"email\":\"admin@admin.com\",\"password\":\"$2y$10$OIZeCJF6vQ.a4g5k93IGEeYhO6K9SFohlb2HWpMYHLlbHyOimWCyW\",\"status\":\"active\",\"timezone\":\"UTC\",\"code\":\"Vv8RKxqJ09\",\"remember_token\":\"aI5XeDkXBV\",\"email_verified_at\":\"2021-07-04T17:23:23.000000Z\",\"created_at\":\"2021-07-04T17:23:23.000000Z\",\"updated_at\":\"2021-07-04T17:23:23.000000Z\",\"deleted_at\":null,\"language\":\"en\"}}', '2021-07-04 11:53:24', '2021-07-04 11:53:24'),
(2, 'default', 'created', 'App\\Models\\BusinessOwner', 1, NULL, NULL, '{\"attributes\":{\"id\":1,\"name\":\"Ms. Laila Rodriguez DDS\",\"email\":\"torrey.weber@example.net\",\"password\":\"$2y$10$9uCw9hr00P9cuPr2\\/jMASuJUb7SQHx.hxRpAJRq1jFYV0czELHAya\",\"status\":\"active\",\"timezone\":\"UTC\",\"code\":\"jTRDTvvy6E\",\"remember_token\":\"tBygeDCtKn\",\"email_verified_at\":\"2021-07-04T17:23:25.000000Z\",\"created_at\":\"2021-07-04T17:23:25.000000Z\",\"updated_at\":\"2021-07-04T17:23:25.000000Z\",\"deleted_at\":null,\"language\":\"en\"}}', '2021-07-04 11:53:25', '2021-07-04 11:53:25'),
(3, 'default', 'created', 'App\\Models\\Business', 1, NULL, NULL, '{\"attributes\":{\"id\":1,\"name\":\"sfd\",\"business_owner_id\":2,\"logo\":null,\"timezone\":\"Africa\\/Bujumbura\",\"code\":\"MQB6ZjWm2X\",\"status\":\"active\",\"created_at\":\"2021-07-04T17:29:39.000000Z\",\"updated_at\":\"2021-07-04T17:29:39.000000Z\",\"deleted_at\":null,\"language\":\"Italian\",\"description\":\"sdf\"}}', '2021-07-04 11:59:39', '2021-07-04 11:59:39'),
(4, 'default', 'created', 'App\\Models\\BusinessStaffMember', 1, NULL, NULL, '{\"attributes\":{\"id\":1,\"name\":\"Udit Arora\",\"email\":\"uarora1997@gmail.com\",\"password\":\"$2y$10$\\/X0zFrDZp.F9K5EPWJ657.oHqnrWjQ4MNM8sMCfWhrN7OCzQuEYMi\",\"status\":\"active\",\"zoom_active\":\"\",\"zoom_refresh\":\"\",\"paypal_token\":\"\",\"stripe_token\":\"\",\"code\":\"szq3kWrcV8\",\"timezone\":\"America\\/Nipigon\",\"language\":\"\",\"date_format\":\"\",\"time_format\":\"\",\"business_id\":1,\"created_at\":\"2021-07-04T17:30:46.000000Z\",\"updated_at\":\"2021-07-04T17:30:46.000000Z\",\"deleted_at\":null,\"remember_token\":null,\"email_verified_at\":null,\"profile\":null,\"logo\":null,\"description\":\"uarora1997@gmail.com\",\"g_selected_email\":null,\"g_calendar_id\":null}}', '2021-07-04 12:00:46', '2021-07-04 12:00:46'),
(5, 'default', 'created', 'App\\Models\\StaffSchedule', 1, NULL, NULL, '{\"attributes\":{\"id\":1,\"name\":\"adsf\",\"staff_id\":1,\"is_default\":\"no\",\"created_at\":\"2021-07-04T17:31:44.000000Z\",\"updated_at\":\"2021-07-04T17:31:44.000000Z\",\"monday\":\"{\\\"start_time\\\":[\\\"9:00\\\"],\\\"end_time\\\":[\\\"10:00\\\"]}\",\"tuesday\":null,\"wednesday\":null,\"thursday\":null,\"friday\":null,\"saturday\":null,\"sunday\":null}}', '2021-07-04 12:01:44', '2021-07-04 12:01:44'),
(6, 'default', 'updated', 'App\\Models\\BusinessStaffMember', 1, NULL, NULL, '{\"attributes\":{\"id\":1,\"name\":\"Udit Arora\",\"email\":\"uarora1997@gmail.com\",\"password\":\"$2y$10$\\/X0zFrDZp.F9K5EPWJ657.oHqnrWjQ4MNM8sMCfWhrN7OCzQuEYMi\",\"status\":\"active\",\"zoom_active\":\"\",\"zoom_refresh\":\"\",\"paypal_token\":\"\",\"stripe_token\":\"\",\"code\":\"szq3kWrcV8\",\"timezone\":\"America\\/Nipigon\",\"language\":\"\",\"date_format\":\"\",\"time_format\":\"\",\"business_id\":1,\"created_at\":\"2021-07-04T17:30:46.000000Z\",\"updated_at\":\"2021-07-04T17:33:18.000000Z\",\"deleted_at\":null,\"remember_token\":null,\"email_verified_at\":null,\"profile\":null,\"logo\":null,\"description\":\"uarora1997@gmail.com\",\"g_selected_email\":\"uditarora.ubs@gmail.com\",\"g_calendar_id\":null},\"old\":{\"id\":1,\"name\":\"Udit Arora\",\"email\":\"uarora1997@gmail.com\",\"password\":\"$2y$10$\\/X0zFrDZp.F9K5EPWJ657.oHqnrWjQ4MNM8sMCfWhrN7OCzQuEYMi\",\"status\":\"active\",\"zoom_active\":\"\",\"zoom_refresh\":\"\",\"paypal_token\":\"\",\"stripe_token\":\"\",\"code\":\"szq3kWrcV8\",\"timezone\":\"America\\/Nipigon\",\"language\":\"\",\"date_format\":\"\",\"time_format\":\"\",\"business_id\":1,\"created_at\":\"2021-07-04T17:30:46.000000Z\",\"updated_at\":\"2021-07-04T17:30:46.000000Z\",\"deleted_at\":null,\"remember_token\":null,\"email_verified_at\":null,\"profile\":null,\"logo\":null,\"description\":\"uarora1997@gmail.com\",\"g_selected_email\":null,\"g_calendar_id\":null}}', '2021-07-04 12:03:18', '2021-07-04 12:03:18'),
(7, 'default', 'updated', 'App\\Models\\BusinessStaffMember', 1, NULL, NULL, '{\"attributes\":{\"id\":1,\"name\":\"Udit Arora\",\"email\":\"uarora1997@gmail.com\",\"password\":\"$2y$10$\\/X0zFrDZp.F9K5EPWJ657.oHqnrWjQ4MNM8sMCfWhrN7OCzQuEYMi\",\"status\":\"active\",\"zoom_active\":\"\",\"zoom_refresh\":\"\",\"paypal_token\":\"\",\"stripe_token\":\"\",\"code\":\"szq3kWrcV8\",\"timezone\":\"Africa\\/Abidjan\",\"language\":\"Italian\",\"date_format\":\"MM\\/DD\\/YYYY\",\"time_format\":\"12h (am\\/pm)\",\"business_id\":1,\"created_at\":\"2021-07-04T17:30:46.000000Z\",\"updated_at\":\"2021-07-04T17:41:50.000000Z\",\"deleted_at\":null,\"remember_token\":null,\"email_verified_at\":null,\"profile\":null,\"logo\":null,\"description\":\"uarora1997@gmail.com\",\"g_selected_email\":\"uditarora.ubs@gmail.com\",\"g_calendar_id\":null},\"old\":{\"id\":1,\"name\":\"Udit Arora\",\"email\":\"uarora1997@gmail.com\",\"password\":\"$2y$10$\\/X0zFrDZp.F9K5EPWJ657.oHqnrWjQ4MNM8sMCfWhrN7OCzQuEYMi\",\"status\":\"active\",\"zoom_active\":\"\",\"zoom_refresh\":\"\",\"paypal_token\":\"\",\"stripe_token\":\"\",\"code\":\"szq3kWrcV8\",\"timezone\":\"America\\/Nipigon\",\"language\":\"\",\"date_format\":\"\",\"time_format\":\"\",\"business_id\":1,\"created_at\":\"2021-07-04T17:30:46.000000Z\",\"updated_at\":\"2021-07-04T17:33:18.000000Z\",\"deleted_at\":null,\"remember_token\":null,\"email_verified_at\":null,\"profile\":null,\"logo\":null,\"description\":\"uarora1997@gmail.com\",\"g_selected_email\":\"uditarora.ubs@gmail.com\",\"g_calendar_id\":null}}', '2021-07-04 12:11:51', '2021-07-04 12:11:51'),
(8, 'default', 'updated', 'App\\Models\\BusinessStaffMember', 1, NULL, NULL, '{\"attributes\":{\"id\":1,\"name\":\"Udit Arora\",\"email\":\"uarora1997@gmail.com\",\"password\":\"$2y$10$\\/X0zFrDZp.F9K5EPWJ657.oHqnrWjQ4MNM8sMCfWhrN7OCzQuEYMi\",\"status\":\"active\",\"zoom_active\":\"\",\"zoom_refresh\":\"\",\"paypal_token\":\"\",\"stripe_token\":\"\",\"code\":\"szq3kWrcV8\",\"timezone\":\"Africa\\/Abidjan\",\"language\":\"Italian\",\"date_format\":\"MM\\/DD\\/YYYY\",\"time_format\":\"24h\",\"business_id\":1,\"created_at\":\"2021-07-04T17:30:46.000000Z\",\"updated_at\":\"2021-07-04T17:41:55.000000Z\",\"deleted_at\":null,\"remember_token\":null,\"email_verified_at\":null,\"profile\":null,\"logo\":null,\"description\":\"uarora1997@gmail.com\",\"g_selected_email\":\"uditarora.ubs@gmail.com\",\"g_calendar_id\":null},\"old\":{\"id\":1,\"name\":\"Udit Arora\",\"email\":\"uarora1997@gmail.com\",\"password\":\"$2y$10$\\/X0zFrDZp.F9K5EPWJ657.oHqnrWjQ4MNM8sMCfWhrN7OCzQuEYMi\",\"status\":\"active\",\"zoom_active\":\"\",\"zoom_refresh\":\"\",\"paypal_token\":\"\",\"stripe_token\":\"\",\"code\":\"szq3kWrcV8\",\"timezone\":\"Africa\\/Abidjan\",\"language\":\"Italian\",\"date_format\":\"MM\\/DD\\/YYYY\",\"time_format\":\"12h (am\\/pm)\",\"business_id\":1,\"created_at\":\"2021-07-04T17:30:46.000000Z\",\"updated_at\":\"2021-07-04T17:41:50.000000Z\",\"deleted_at\":null,\"remember_token\":null,\"email_verified_at\":null,\"profile\":null,\"logo\":null,\"description\":\"uarora1997@gmail.com\",\"g_selected_email\":\"uditarora.ubs@gmail.com\",\"g_calendar_id\":null}}', '2021-07-04 12:11:55', '2021-07-04 12:11:55'),
(9, 'default', 'updated', 'App\\Models\\BusinessStaffMember', 1, NULL, NULL, '{\"attributes\":{\"id\":1,\"name\":\"Udit Arora\",\"email\":\"uarora1997@gmail.com\",\"password\":\"$2y$10$\\/X0zFrDZp.F9K5EPWJ657.oHqnrWjQ4MNM8sMCfWhrN7OCzQuEYMi\",\"status\":\"active\",\"zoom_active\":\"\",\"zoom_refresh\":\"\",\"paypal_token\":\"\",\"stripe_token\":\"\",\"code\":\"szq3kWrcV8\",\"timezone\":\"Africa\\/Abidjan\",\"language\":\"Italian\",\"date_format\":\"DD\\/MM\\/YYYY\",\"time_format\":\"12h (am\\/pm)\",\"business_id\":1,\"created_at\":\"2021-07-04T17:30:46.000000Z\",\"updated_at\":\"2021-07-04T17:41:59.000000Z\",\"deleted_at\":null,\"remember_token\":null,\"email_verified_at\":null,\"profile\":null,\"logo\":null,\"description\":\"uarora1997@gmail.com\",\"g_selected_email\":\"uditarora.ubs@gmail.com\",\"g_calendar_id\":null},\"old\":{\"id\":1,\"name\":\"Udit Arora\",\"email\":\"uarora1997@gmail.com\",\"password\":\"$2y$10$\\/X0zFrDZp.F9K5EPWJ657.oHqnrWjQ4MNM8sMCfWhrN7OCzQuEYMi\",\"status\":\"active\",\"zoom_active\":\"\",\"zoom_refresh\":\"\",\"paypal_token\":\"\",\"stripe_token\":\"\",\"code\":\"szq3kWrcV8\",\"timezone\":\"Africa\\/Abidjan\",\"language\":\"Italian\",\"date_format\":\"MM\\/DD\\/YYYY\",\"time_format\":\"24h\",\"business_id\":1,\"created_at\":\"2021-07-04T17:30:46.000000Z\",\"updated_at\":\"2021-07-04T17:41:55.000000Z\",\"deleted_at\":null,\"remember_token\":null,\"email_verified_at\":null,\"profile\":null,\"logo\":null,\"description\":\"uarora1997@gmail.com\",\"g_selected_email\":\"uditarora.ubs@gmail.com\",\"g_calendar_id\":null}}', '2021-07-04 12:11:59', '2021-07-04 12:11:59'),
(10, 'default', 'updated', 'App\\Models\\BusinessStaffMember', 1, NULL, NULL, '{\"attributes\":{\"id\":1,\"name\":\"Udit Arora\",\"email\":\"uarora1997@gmail.com\",\"password\":\"$2y$10$\\/X0zFrDZp.F9K5EPWJ657.oHqnrWjQ4MNM8sMCfWhrN7OCzQuEYMi\",\"status\":\"active\",\"zoom_active\":\"\",\"zoom_refresh\":\"\",\"paypal_token\":\"\",\"stripe_token\":\"\",\"code\":\"szq3kWrcV8\",\"timezone\":\"America\\/Blanc-Sablon\",\"language\":\"Italian\",\"date_format\":\"MM\\/DD\\/YYYY\",\"time_format\":\"12h (am\\/pm)\",\"business_id\":1,\"created_at\":\"2021-07-04T17:30:46.000000Z\",\"updated_at\":\"2021-07-04T17:42:06.000000Z\",\"deleted_at\":null,\"remember_token\":null,\"email_verified_at\":null,\"profile\":null,\"logo\":null,\"description\":\"uarora1997@gmail.com\",\"g_selected_email\":\"uditarora.ubs@gmail.com\",\"g_calendar_id\":null},\"old\":{\"id\":1,\"name\":\"Udit Arora\",\"email\":\"uarora1997@gmail.com\",\"password\":\"$2y$10$\\/X0zFrDZp.F9K5EPWJ657.oHqnrWjQ4MNM8sMCfWhrN7OCzQuEYMi\",\"status\":\"active\",\"zoom_active\":\"\",\"zoom_refresh\":\"\",\"paypal_token\":\"\",\"stripe_token\":\"\",\"code\":\"szq3kWrcV8\",\"timezone\":\"Africa\\/Abidjan\",\"language\":\"Italian\",\"date_format\":\"DD\\/MM\\/YYYY\",\"time_format\":\"12h (am\\/pm)\",\"business_id\":1,\"created_at\":\"2021-07-04T17:30:46.000000Z\",\"updated_at\":\"2021-07-04T17:41:59.000000Z\",\"deleted_at\":null,\"remember_token\":null,\"email_verified_at\":null,\"profile\":null,\"logo\":null,\"description\":\"uarora1997@gmail.com\",\"g_selected_email\":\"uditarora.ubs@gmail.com\",\"g_calendar_id\":null}}', '2021-07-04 12:12:06', '2021-07-04 12:12:06'),
(11, 'default', 'updated', 'App\\Models\\BusinessStaffMember', 1, NULL, NULL, '{\"attributes\":{\"id\":1,\"name\":\"Udit Arora\",\"email\":\"uarora1997@gmail.com\",\"password\":\"$2y$10$\\/X0zFrDZp.F9K5EPWJ657.oHqnrWjQ4MNM8sMCfWhrN7OCzQuEYMi\",\"status\":\"active\",\"zoom_active\":\"\",\"zoom_refresh\":\"\",\"paypal_token\":\"\",\"stripe_token\":\"\",\"code\":\"szq3kWrcV8\",\"timezone\":\"Arctic\\/Longyearbyen\",\"language\":\"Italian\",\"date_format\":\"MM\\/DD\\/YYYY\",\"time_format\":\"12h (am\\/pm)\",\"business_id\":1,\"created_at\":\"2021-07-04T17:30:46.000000Z\",\"updated_at\":\"2021-07-04T17:42:11.000000Z\",\"deleted_at\":null,\"remember_token\":null,\"email_verified_at\":null,\"profile\":null,\"logo\":null,\"description\":\"uarora1997@gmail.com\",\"g_selected_email\":\"uditarora.ubs@gmail.com\",\"g_calendar_id\":null},\"old\":{\"id\":1,\"name\":\"Udit Arora\",\"email\":\"uarora1997@gmail.com\",\"password\":\"$2y$10$\\/X0zFrDZp.F9K5EPWJ657.oHqnrWjQ4MNM8sMCfWhrN7OCzQuEYMi\",\"status\":\"active\",\"zoom_active\":\"\",\"zoom_refresh\":\"\",\"paypal_token\":\"\",\"stripe_token\":\"\",\"code\":\"szq3kWrcV8\",\"timezone\":\"America\\/Blanc-Sablon\",\"language\":\"Italian\",\"date_format\":\"MM\\/DD\\/YYYY\",\"time_format\":\"12h (am\\/pm)\",\"business_id\":1,\"created_at\":\"2021-07-04T17:30:46.000000Z\",\"updated_at\":\"2021-07-04T17:42:06.000000Z\",\"deleted_at\":null,\"remember_token\":null,\"email_verified_at\":null,\"profile\":null,\"logo\":null,\"description\":\"uarora1997@gmail.com\",\"g_selected_email\":\"uditarora.ubs@gmail.com\",\"g_calendar_id\":null}}', '2021-07-04 12:12:11', '2021-07-04 12:12:11'),
(12, 'default', 'updated', 'App\\Models\\BusinessStaffMember', 1, NULL, NULL, '{\"attributes\":{\"id\":1,\"name\":\"Udit Arora\",\"email\":\"uarora1997@gmail.com\",\"password\":\"$2y$10$\\/X0zFrDZp.F9K5EPWJ657.oHqnrWjQ4MNM8sMCfWhrN7OCzQuEYMi\",\"status\":\"active\",\"zoom_active\":\"\",\"zoom_refresh\":\"\",\"paypal_token\":\"\",\"stripe_token\":\"\",\"code\":\"szq3kWrcV8\",\"timezone\":\"Asia\\/Yakutsk\",\"language\":\"Italian\",\"date_format\":\"MM\\/DD\\/YYYY\",\"time_format\":\"12h (am\\/pm)\",\"business_id\":1,\"created_at\":\"2021-07-04T17:30:46.000000Z\",\"updated_at\":\"2021-07-04T17:42:16.000000Z\",\"deleted_at\":null,\"remember_token\":null,\"email_verified_at\":null,\"profile\":null,\"logo\":null,\"description\":\"uarora1997@gmail.com\",\"g_selected_email\":\"uditarora.ubs@gmail.com\",\"g_calendar_id\":null},\"old\":{\"id\":1,\"name\":\"Udit Arora\",\"email\":\"uarora1997@gmail.com\",\"password\":\"$2y$10$\\/X0zFrDZp.F9K5EPWJ657.oHqnrWjQ4MNM8sMCfWhrN7OCzQuEYMi\",\"status\":\"active\",\"zoom_active\":\"\",\"zoom_refresh\":\"\",\"paypal_token\":\"\",\"stripe_token\":\"\",\"code\":\"szq3kWrcV8\",\"timezone\":\"Arctic\\/Longyearbyen\",\"language\":\"Italian\",\"date_format\":\"MM\\/DD\\/YYYY\",\"time_format\":\"12h (am\\/pm)\",\"business_id\":1,\"created_at\":\"2021-07-04T17:30:46.000000Z\",\"updated_at\":\"2021-07-04T17:42:11.000000Z\",\"deleted_at\":null,\"remember_token\":null,\"email_verified_at\":null,\"profile\":null,\"logo\":null,\"description\":\"uarora1997@gmail.com\",\"g_selected_email\":\"uditarora.ubs@gmail.com\",\"g_calendar_id\":null}}', '2021-07-04 12:12:16', '2021-07-04 12:12:16'),
(13, 'default', 'updated', 'App\\Models\\BusinessStaffMember', 1, NULL, NULL, '{\"attributes\":{\"id\":1,\"name\":\"Udit Arora\",\"email\":\"uarora1997@gmail.com\",\"password\":\"$2y$10$\\/X0zFrDZp.F9K5EPWJ657.oHqnrWjQ4MNM8sMCfWhrN7OCzQuEYMi\",\"status\":\"active\",\"zoom_active\":\"\",\"zoom_refresh\":\"\",\"paypal_token\":\"\",\"stripe_token\":\"\",\"code\":\"szq3kWrcV8\",\"timezone\":\"Europe\\/Helsinki\",\"language\":\"Czech\",\"date_format\":\"MM\\/DD\\/YYYY\",\"time_format\":\"12h (am\\/pm)\",\"business_id\":1,\"created_at\":\"2021-07-04T17:30:46.000000Z\",\"updated_at\":\"2021-07-04T17:44:45.000000Z\",\"deleted_at\":null,\"remember_token\":null,\"email_verified_at\":null,\"profile\":null,\"logo\":null,\"description\":\"uarora1997@gmail.com\",\"g_selected_email\":\"uditarora.ubs@gmail.com\",\"g_calendar_id\":null},\"old\":{\"id\":1,\"name\":\"Udit Arora\",\"email\":\"uarora1997@gmail.com\",\"password\":\"$2y$10$\\/X0zFrDZp.F9K5EPWJ657.oHqnrWjQ4MNM8sMCfWhrN7OCzQuEYMi\",\"status\":\"active\",\"zoom_active\":\"\",\"zoom_refresh\":\"\",\"paypal_token\":\"\",\"stripe_token\":\"\",\"code\":\"szq3kWrcV8\",\"timezone\":\"Asia\\/Yakutsk\",\"language\":\"Italian\",\"date_format\":\"MM\\/DD\\/YYYY\",\"time_format\":\"12h (am\\/pm)\",\"business_id\":1,\"created_at\":\"2021-07-04T17:30:46.000000Z\",\"updated_at\":\"2021-07-04T17:42:16.000000Z\",\"deleted_at\":null,\"remember_token\":null,\"email_verified_at\":null,\"profile\":null,\"logo\":null,\"description\":\"uarora1997@gmail.com\",\"g_selected_email\":\"uditarora.ubs@gmail.com\",\"g_calendar_id\":null}}', '2021-07-04 12:14:45', '2021-07-04 12:14:45'),
(14, 'default', 'updated', 'App\\Models\\BusinessStaffMember', 1, NULL, NULL, '{\"attributes\":{\"id\":1,\"name\":\"Udit Arora\",\"email\":\"uarora1997@gmail.com\",\"password\":\"$2y$10$\\/X0zFrDZp.F9K5EPWJ657.oHqnrWjQ4MNM8sMCfWhrN7OCzQuEYMi\",\"status\":\"active\",\"zoom_active\":\"\",\"zoom_refresh\":\"\",\"paypal_token\":\"\",\"stripe_token\":\"\",\"code\":\"szq3kWrcV8\",\"timezone\":\"Africa\\/Abidjan\",\"language\":\"Czech\",\"date_format\":\"MM\\/DD\\/YYYY\",\"time_format\":\"12h (am\\/pm)\",\"business_id\":1,\"created_at\":\"2021-07-04T17:30:46.000000Z\",\"updated_at\":\"2021-07-05T11:37:23.000000Z\",\"deleted_at\":null,\"remember_token\":null,\"email_verified_at\":null,\"profile\":null,\"logo\":null,\"description\":\"uarora1997@gmail.com\",\"g_selected_email\":\"uditarora.ubs@gmail.com\",\"g_calendar_id\":null},\"old\":{\"id\":1,\"name\":\"Udit Arora\",\"email\":\"uarora1997@gmail.com\",\"password\":\"$2y$10$\\/X0zFrDZp.F9K5EPWJ657.oHqnrWjQ4MNM8sMCfWhrN7OCzQuEYMi\",\"status\":\"active\",\"zoom_active\":\"\",\"zoom_refresh\":\"\",\"paypal_token\":\"\",\"stripe_token\":\"\",\"code\":\"szq3kWrcV8\",\"timezone\":\"Europe\\/Helsinki\",\"language\":\"Czech\",\"date_format\":\"MM\\/DD\\/YYYY\",\"time_format\":\"12h (am\\/pm)\",\"business_id\":1,\"created_at\":\"2021-07-04T17:30:46.000000Z\",\"updated_at\":\"2021-07-04T17:44:45.000000Z\",\"deleted_at\":null,\"remember_token\":null,\"email_verified_at\":null,\"profile\":null,\"logo\":null,\"description\":\"uarora1997@gmail.com\",\"g_selected_email\":\"uditarora.ubs@gmail.com\",\"g_calendar_id\":null}}', '2021-07-05 06:07:27', '2021-07-05 06:07:27'),
(15, 'default', 'updated', 'App\\Models\\BusinessStaffMember', 1, NULL, NULL, '{\"attributes\":{\"id\":1,\"name\":\"Udit Arora\",\"email\":\"uarora1997@gmail.com\",\"password\":\"$2y$10$\\/X0zFrDZp.F9K5EPWJ657.oHqnrWjQ4MNM8sMCfWhrN7OCzQuEYMi\",\"status\":\"active\",\"zoom_active\":\"\",\"zoom_refresh\":\"\",\"paypal_token\":\"\",\"stripe_token\":\"\",\"code\":\"szq3kWrcV8\",\"timezone\":\"Africa\\/Abidjan\",\"language\":\"Czech\",\"date_format\":\"MM\\/DD\\/YYYY\",\"time_format\":\"24h\",\"business_id\":1,\"created_at\":\"2021-07-04T17:30:46.000000Z\",\"updated_at\":\"2021-07-05T11:37:33.000000Z\",\"deleted_at\":null,\"remember_token\":null,\"email_verified_at\":null,\"profile\":null,\"logo\":null,\"description\":\"uarora1997@gmail.com\",\"g_selected_email\":\"uditarora.ubs@gmail.com\",\"g_calendar_id\":null},\"old\":{\"id\":1,\"name\":\"Udit Arora\",\"email\":\"uarora1997@gmail.com\",\"password\":\"$2y$10$\\/X0zFrDZp.F9K5EPWJ657.oHqnrWjQ4MNM8sMCfWhrN7OCzQuEYMi\",\"status\":\"active\",\"zoom_active\":\"\",\"zoom_refresh\":\"\",\"paypal_token\":\"\",\"stripe_token\":\"\",\"code\":\"szq3kWrcV8\",\"timezone\":\"Africa\\/Abidjan\",\"language\":\"Czech\",\"date_format\":\"MM\\/DD\\/YYYY\",\"time_format\":\"12h (am\\/pm)\",\"business_id\":1,\"created_at\":\"2021-07-04T17:30:46.000000Z\",\"updated_at\":\"2021-07-05T11:37:23.000000Z\",\"deleted_at\":null,\"remember_token\":null,\"email_verified_at\":null,\"profile\":null,\"logo\":null,\"description\":\"uarora1997@gmail.com\",\"g_selected_email\":\"uditarora.ubs@gmail.com\",\"g_calendar_id\":null}}', '2021-07-05 06:07:33', '2021-07-05 06:07:33'),
(16, 'default', 'created', 'App\\Models\\StaffSchedule', 2, NULL, NULL, '{\"attributes\":{\"id\":2,\"name\":\"kuldeeptest\",\"staff_id\":1,\"is_default\":\"no\",\"created_at\":\"2021-07-06T16:18:38.000000Z\",\"updated_at\":\"2021-07-06T16:18:38.000000Z\",\"monday\":null,\"tuesday\":null,\"wednesday\":null,\"thursday\":null,\"friday\":null,\"saturday\":\"{\\\"start_time\\\":[\\\"12:00\\\",\\\"14:00\\\"],\\\"end_time\\\":[\\\"13:00\\\",\\\"15:00\\\"]}\",\"sunday\":null}}', '2021-07-06 10:48:40', '2021-07-06 10:48:40'),
(17, 'default', 'updated', 'App\\Models\\StaffSchedule', 2, NULL, NULL, '{\"attributes\":{\"id\":2,\"name\":\"kuldeeptest\",\"staff_id\":1,\"is_default\":\"no\",\"created_at\":\"2021-07-06T16:18:38.000000Z\",\"updated_at\":\"2021-07-06T16:19:29.000000Z\",\"monday\":\"{\\\"start_time\\\":[\\\"14:00\\\",\\\"18:00\\\"],\\\"end_time\\\":[\\\"15:00\\\",\\\"19:00\\\"]}\",\"tuesday\":null,\"wednesday\":null,\"thursday\":null,\"friday\":null,\"saturday\":\"{\\\"start_time\\\":[\\\"12:00\\\",\\\"14:00\\\"],\\\"end_time\\\":[\\\"13:00\\\",\\\"15:00\\\"]}\",\"sunday\":null},\"old\":{\"id\":2,\"name\":\"kuldeeptest\",\"staff_id\":1,\"is_default\":\"no\",\"created_at\":\"2021-07-06T16:18:38.000000Z\",\"updated_at\":\"2021-07-06T16:18:38.000000Z\",\"monday\":null,\"tuesday\":null,\"wednesday\":null,\"thursday\":null,\"friday\":null,\"saturday\":\"{\\\"start_time\\\":[\\\"12:00\\\",\\\"14:00\\\"],\\\"end_time\\\":[\\\"13:00\\\",\\\"15:00\\\"]}\",\"sunday\":null}}', '2021-07-06 10:49:29', '2021-07-06 10:49:29'),
(18, 'default', 'updated', 'App\\Models\\StaffSchedule', 2, NULL, NULL, '{\"attributes\":{\"id\":2,\"name\":\"kuldeeptest\",\"staff_id\":1,\"is_default\":\"no\",\"created_at\":\"2021-07-06T16:18:38.000000Z\",\"updated_at\":\"2021-07-06T16:19:35.000000Z\",\"monday\":\"{\\\"start_time\\\":[\\\"14:00\\\",\\\"18:00\\\"],\\\"end_time\\\":[\\\"15:00\\\",\\\"19:00\\\"]}\",\"tuesday\":null,\"wednesday\":null,\"thursday\":null,\"friday\":null,\"saturday\":\"{\\\"start_time\\\":[\\\"12:00\\\"],\\\"end_time\\\":[\\\"13:00\\\"]}\",\"sunday\":null},\"old\":{\"id\":2,\"name\":\"kuldeeptest\",\"staff_id\":1,\"is_default\":\"no\",\"created_at\":\"2021-07-06T16:18:38.000000Z\",\"updated_at\":\"2021-07-06T16:19:29.000000Z\",\"monday\":\"{\\\"start_time\\\":[\\\"14:00\\\",\\\"18:00\\\"],\\\"end_time\\\":[\\\"15:00\\\",\\\"19:00\\\"]}\",\"tuesday\":null,\"wednesday\":null,\"thursday\":null,\"friday\":null,\"saturday\":\"{\\\"start_time\\\":[\\\"12:00\\\",\\\"14:00\\\"],\\\"end_time\\\":[\\\"13:00\\\",\\\"15:00\\\"]}\",\"sunday\":null}}', '2021-07-06 10:49:36', '2021-07-06 10:49:36'),
(19, 'default', 'updated', 'App\\Models\\StaffSchedule', 2, NULL, NULL, '{\"attributes\":{\"id\":2,\"name\":\"kuldeeptest\",\"staff_id\":1,\"is_default\":\"no\",\"created_at\":\"2021-07-06T16:18:38.000000Z\",\"updated_at\":\"2021-07-06T16:19:42.000000Z\",\"monday\":\"{\\\"start_time\\\":[\\\"14:00\\\",\\\"18:00\\\"],\\\"end_time\\\":[\\\"15:00\\\",\\\"19:00\\\"]}\",\"tuesday\":null,\"wednesday\":null,\"thursday\":null,\"friday\":null,\"saturday\":\"{\\\"start_time\\\":[\\\"12:00\\\"],\\\"end_time\\\":[\\\"15:00\\\"]}\",\"sunday\":null},\"old\":{\"id\":2,\"name\":\"kuldeeptest\",\"staff_id\":1,\"is_default\":\"no\",\"created_at\":\"2021-07-06T16:18:38.000000Z\",\"updated_at\":\"2021-07-06T16:19:35.000000Z\",\"monday\":\"{\\\"start_time\\\":[\\\"14:00\\\",\\\"18:00\\\"],\\\"end_time\\\":[\\\"15:00\\\",\\\"19:00\\\"]}\",\"tuesday\":null,\"wednesday\":null,\"thursday\":null,\"friday\":null,\"saturday\":\"{\\\"start_time\\\":[\\\"12:00\\\"],\\\"end_time\\\":[\\\"13:00\\\"]}\",\"sunday\":null}}', '2021-07-06 10:49:42', '2021-07-06 10:49:42'),
(20, 'default', 'updated', 'App\\Models\\StaffSchedule', 2, NULL, NULL, '{\"attributes\":{\"id\":2,\"name\":\"kuldeeptest\",\"staff_id\":1,\"is_default\":\"no\",\"created_at\":\"2021-07-06T16:18:38.000000Z\",\"updated_at\":\"2021-07-06T16:22:06.000000Z\",\"monday\":\"{\\\"start_time\\\":[\\\"18:00\\\"],\\\"end_time\\\":[\\\"19:00\\\"]}\",\"tuesday\":null,\"wednesday\":null,\"thursday\":null,\"friday\":null,\"saturday\":\"{\\\"start_time\\\":[\\\"12:00\\\"],\\\"end_time\\\":[\\\"15:00\\\"]}\",\"sunday\":null},\"old\":{\"id\":2,\"name\":\"kuldeeptest\",\"staff_id\":1,\"is_default\":\"no\",\"created_at\":\"2021-07-06T16:18:38.000000Z\",\"updated_at\":\"2021-07-06T16:19:42.000000Z\",\"monday\":\"{\\\"start_time\\\":[\\\"14:00\\\",\\\"18:00\\\"],\\\"end_time\\\":[\\\"15:00\\\",\\\"19:00\\\"]}\",\"tuesday\":null,\"wednesday\":null,\"thursday\":null,\"friday\":null,\"saturday\":\"{\\\"start_time\\\":[\\\"12:00\\\"],\\\"end_time\\\":[\\\"15:00\\\"]}\",\"sunday\":null}}', '2021-07-06 10:52:06', '2021-07-06 10:52:06'),
(21, 'default', 'updated', 'App\\Models\\StaffSchedule', 2, NULL, NULL, '{\"attributes\":{\"id\":2,\"name\":\"kuldeeptest\",\"staff_id\":1,\"is_default\":\"no\",\"created_at\":\"2021-07-06T16:18:38.000000Z\",\"updated_at\":\"2021-07-06T16:23:06.000000Z\",\"monday\":\"{\\\"start_time\\\":[\\\"18:00\\\"],\\\"end_time\\\":[\\\"19:00\\\"]}\",\"tuesday\":\"{\\\"start_time\\\":[\\\"12:00\\\"],\\\"end_time\\\":[\\\"15:00\\\"]}\",\"wednesday\":null,\"thursday\":null,\"friday\":null,\"saturday\":\"{\\\"start_time\\\":[\\\"12:00\\\"],\\\"end_time\\\":[\\\"15:00\\\"]}\",\"sunday\":null},\"old\":{\"id\":2,\"name\":\"kuldeeptest\",\"staff_id\":1,\"is_default\":\"no\",\"created_at\":\"2021-07-06T16:18:38.000000Z\",\"updated_at\":\"2021-07-06T16:22:06.000000Z\",\"monday\":\"{\\\"start_time\\\":[\\\"18:00\\\"],\\\"end_time\\\":[\\\"19:00\\\"]}\",\"tuesday\":null,\"wednesday\":null,\"thursday\":null,\"friday\":null,\"saturday\":\"{\\\"start_time\\\":[\\\"12:00\\\"],\\\"end_time\\\":[\\\"15:00\\\"]}\",\"sunday\":null}}', '2021-07-06 10:53:06', '2021-07-06 10:53:06'),
(22, 'default', 'updated', 'App\\Models\\StaffSchedule', 2, NULL, NULL, '{\"attributes\":{\"id\":2,\"name\":\"kuldeeptest\",\"staff_id\":1,\"is_default\":\"no\",\"created_at\":\"2021-07-06T16:18:38.000000Z\",\"updated_at\":\"2021-07-06T16:23:24.000000Z\",\"monday\":\"{\\\"start_time\\\":[\\\"18:00\\\"],\\\"end_time\\\":[\\\"19:00\\\"]}\",\"tuesday\":\"{\\\"start_time\\\":[\\\"12:00\\\"],\\\"end_time\\\":[\\\"14:00\\\"]}\",\"wednesday\":null,\"thursday\":null,\"friday\":null,\"saturday\":\"{\\\"start_time\\\":[\\\"12:00\\\"],\\\"end_time\\\":[\\\"15:00\\\"]}\",\"sunday\":null},\"old\":{\"id\":2,\"name\":\"kuldeeptest\",\"staff_id\":1,\"is_default\":\"no\",\"created_at\":\"2021-07-06T16:18:38.000000Z\",\"updated_at\":\"2021-07-06T16:23:06.000000Z\",\"monday\":\"{\\\"start_time\\\":[\\\"18:00\\\"],\\\"end_time\\\":[\\\"19:00\\\"]}\",\"tuesday\":\"{\\\"start_time\\\":[\\\"12:00\\\"],\\\"end_time\\\":[\\\"15:00\\\"]}\",\"wednesday\":null,\"thursday\":null,\"friday\":null,\"saturday\":\"{\\\"start_time\\\":[\\\"12:00\\\"],\\\"end_time\\\":[\\\"15:00\\\"]}\",\"sunday\":null}}', '2021-07-06 10:53:24', '2021-07-06 10:53:24'),
(23, 'default', 'created', 'App\\Models\\StaffSchedule', 3, NULL, NULL, '{\"attributes\":{\"id\":3,\"name\":\"sda\",\"staff_id\":1,\"is_default\":\"no\",\"created_at\":\"2021-07-06T16:23:54.000000Z\",\"updated_at\":\"2021-07-06T16:23:54.000000Z\",\"monday\":\"{\\\"start_time\\\":[\\\"12:00\\\"],\\\"end_time\\\":[\\\"15:00\\\"]}\",\"tuesday\":null,\"wednesday\":null,\"thursday\":null,\"friday\":null,\"saturday\":null,\"sunday\":null}}', '2021-07-06 10:53:55', '2021-07-06 10:53:55'),
(24, 'default', 'updated', 'App\\Models\\BusinessStaffMember', 1, NULL, NULL, '{\"attributes\":{\"id\":1,\"name\":\"Udit Arora\",\"email\":\"uarora1997@gmail.com\",\"password\":\"$2y$10$\\/X0zFrDZp.F9K5EPWJ657.oHqnrWjQ4MNM8sMCfWhrN7OCzQuEYMi\",\"status\":\"active\",\"zoom_active\":\"\",\"zoom_refresh\":\"\",\"paypal_token\":\"\",\"stripe_token\":\"\",\"code\":\"szq3kWrcV8\",\"timezone\":\"America\\/Boise\",\"language\":\"English\",\"date_format\":\"MM\\/DD\\/YYYY\",\"time_format\":\"12h (am\\/pm)\",\"business_id\":1,\"created_at\":\"2021-07-04T17:30:46.000000Z\",\"updated_at\":\"2021-07-08T12:40:53.000000Z\",\"deleted_at\":null,\"remember_token\":null,\"email_verified_at\":null,\"profile\":null,\"logo\":null,\"description\":\"uarora1997@gmail.com\",\"g_selected_email\":\"uditarora.ubs@gmail.com\",\"g_calendar_id\":null},\"old\":{\"id\":1,\"name\":\"Udit Arora\",\"email\":\"uarora1997@gmail.com\",\"password\":\"$2y$10$\\/X0zFrDZp.F9K5EPWJ657.oHqnrWjQ4MNM8sMCfWhrN7OCzQuEYMi\",\"status\":\"active\",\"zoom_active\":\"\",\"zoom_refresh\":\"\",\"paypal_token\":\"\",\"stripe_token\":\"\",\"code\":\"szq3kWrcV8\",\"timezone\":\"Africa\\/Abidjan\",\"language\":\"Czech\",\"date_format\":\"MM\\/DD\\/YYYY\",\"time_format\":\"24h\",\"business_id\":1,\"created_at\":\"2021-07-04T17:30:46.000000Z\",\"updated_at\":\"2021-07-05T11:37:33.000000Z\",\"deleted_at\":null,\"remember_token\":null,\"email_verified_at\":null,\"profile\":null,\"logo\":null,\"description\":\"uarora1997@gmail.com\",\"g_selected_email\":\"uditarora.ubs@gmail.com\",\"g_calendar_id\":null}}', '2021-07-08 07:10:54', '2021-07-08 07:10:54'),
(25, 'default', 'updated', 'App\\Models\\BusinessStaffMember', 1, NULL, NULL, '{\"attributes\":{\"id\":1,\"name\":\"Udit Arora\",\"email\":\"uarora1997@gmail.com\",\"password\":\"$2y$10$\\/X0zFrDZp.F9K5EPWJ657.oHqnrWjQ4MNM8sMCfWhrN7OCzQuEYMi\",\"status\":\"active\",\"zoom_active\":\"\",\"zoom_refresh\":\"\",\"paypal_token\":\"\",\"stripe_token\":\"\",\"code\":\"szq3kWrcV8\",\"timezone\":\"Africa\\/Abidjan\",\"language\":\"English\",\"date_format\":\"MM\\/DD\\/YYYY\",\"time_format\":\"24h\",\"business_id\":1,\"created_at\":\"2021-07-04T17:30:46.000000Z\",\"updated_at\":\"2021-07-09T05:17:53.000000Z\",\"deleted_at\":null,\"remember_token\":null,\"email_verified_at\":null,\"profile\":null,\"logo\":null,\"description\":\"uarora1997@gmail.com\",\"g_selected_email\":\"uditarora.ubs@gmail.com\",\"g_calendar_id\":null},\"old\":{\"id\":1,\"name\":\"Udit Arora\",\"email\":\"uarora1997@gmail.com\",\"password\":\"$2y$10$\\/X0zFrDZp.F9K5EPWJ657.oHqnrWjQ4MNM8sMCfWhrN7OCzQuEYMi\",\"status\":\"active\",\"zoom_active\":\"\",\"zoom_refresh\":\"\",\"paypal_token\":\"\",\"stripe_token\":\"\",\"code\":\"szq3kWrcV8\",\"timezone\":\"America\\/Boise\",\"language\":\"English\",\"date_format\":\"MM\\/DD\\/YYYY\",\"time_format\":\"12h (am\\/pm)\",\"business_id\":1,\"created_at\":\"2021-07-04T17:30:46.000000Z\",\"updated_at\":\"2021-07-08T12:40:53.000000Z\",\"deleted_at\":null,\"remember_token\":null,\"email_verified_at\":null,\"profile\":null,\"logo\":null,\"description\":\"uarora1997@gmail.com\",\"g_selected_email\":\"uditarora.ubs@gmail.com\",\"g_calendar_id\":null}}', '2021-07-08 23:47:54', '2021-07-08 23:47:54'),
(26, 'default', 'updated', 'App\\Models\\BusinessStaffMember', 1, NULL, NULL, '{\"attributes\":{\"id\":1,\"name\":\"Udit Arora\",\"email\":\"uarora1997@gmail.com\",\"password\":\"$2y$10$\\/X0zFrDZp.F9K5EPWJ657.oHqnrWjQ4MNM8sMCfWhrN7OCzQuEYMi\",\"status\":\"active\",\"zoom_active\":\"\",\"zoom_refresh\":\"\",\"paypal_token\":\"\",\"stripe_token\":\"\",\"code\":\"szq3kWrcV8\",\"timezone\":\"Africa\\/Abidjan\",\"language\":\"English\",\"date_format\":\"DD\\/MM\\/YYYY\",\"time_format\":\"12h (am\\/pm)\",\"business_id\":1,\"created_at\":\"2021-07-04T17:30:46.000000Z\",\"updated_at\":\"2021-07-09T05:18:07.000000Z\",\"deleted_at\":null,\"remember_token\":null,\"email_verified_at\":null,\"profile\":null,\"logo\":null,\"description\":\"uarora1997@gmail.com\",\"g_selected_email\":\"uditarora.ubs@gmail.com\",\"g_calendar_id\":null},\"old\":{\"id\":1,\"name\":\"Udit Arora\",\"email\":\"uarora1997@gmail.com\",\"password\":\"$2y$10$\\/X0zFrDZp.F9K5EPWJ657.oHqnrWjQ4MNM8sMCfWhrN7OCzQuEYMi\",\"status\":\"active\",\"zoom_active\":\"\",\"zoom_refresh\":\"\",\"paypal_token\":\"\",\"stripe_token\":\"\",\"code\":\"szq3kWrcV8\",\"timezone\":\"Africa\\/Abidjan\",\"language\":\"English\",\"date_format\":\"MM\\/DD\\/YYYY\",\"time_format\":\"24h\",\"business_id\":1,\"created_at\":\"2021-07-04T17:30:46.000000Z\",\"updated_at\":\"2021-07-09T05:17:53.000000Z\",\"deleted_at\":null,\"remember_token\":null,\"email_verified_at\":null,\"profile\":null,\"logo\":null,\"description\":\"uarora1997@gmail.com\",\"g_selected_email\":\"uditarora.ubs@gmail.com\",\"g_calendar_id\":null}}', '2021-07-08 23:48:07', '2021-07-08 23:48:07'),
(27, 'default', 'updated', 'App\\Models\\BusinessStaffMember', 1, NULL, NULL, '{\"attributes\":{\"id\":1,\"name\":\"Udit Arora\",\"email\":\"uarora1997@gmail.com\",\"password\":\"$2y$10$\\/X0zFrDZp.F9K5EPWJ657.oHqnrWjQ4MNM8sMCfWhrN7OCzQuEYMi\",\"status\":\"active\",\"zoom_active\":\"\",\"zoom_refresh\":\"\",\"paypal_token\":\"\",\"stripe_token\":\"\",\"code\":\"szq3kWrcV8\",\"timezone\":\"Africa\\/Abidjan\",\"language\":\"German\",\"date_format\":\"MM\\/DD\\/YYYY\",\"time_format\":\"12h (am\\/pm)\",\"business_id\":1,\"created_at\":\"2021-07-04T17:30:46.000000Z\",\"updated_at\":\"2021-07-09T05:18:14.000000Z\",\"deleted_at\":null,\"remember_token\":null,\"email_verified_at\":null,\"profile\":null,\"logo\":null,\"description\":\"uarora1997@gmail.com\",\"g_selected_email\":\"uditarora.ubs@gmail.com\",\"g_calendar_id\":null},\"old\":{\"id\":1,\"name\":\"Udit Arora\",\"email\":\"uarora1997@gmail.com\",\"password\":\"$2y$10$\\/X0zFrDZp.F9K5EPWJ657.oHqnrWjQ4MNM8sMCfWhrN7OCzQuEYMi\",\"status\":\"active\",\"zoom_active\":\"\",\"zoom_refresh\":\"\",\"paypal_token\":\"\",\"stripe_token\":\"\",\"code\":\"szq3kWrcV8\",\"timezone\":\"Africa\\/Abidjan\",\"language\":\"English\",\"date_format\":\"DD\\/MM\\/YYYY\",\"time_format\":\"12h (am\\/pm)\",\"business_id\":1,\"created_at\":\"2021-07-04T17:30:46.000000Z\",\"updated_at\":\"2021-07-09T05:18:07.000000Z\",\"deleted_at\":null,\"remember_token\":null,\"email_verified_at\":null,\"profile\":null,\"logo\":null,\"description\":\"uarora1997@gmail.com\",\"g_selected_email\":\"uditarora.ubs@gmail.com\",\"g_calendar_id\":null}}', '2021-07-08 23:48:14', '2021-07-08 23:48:14'),
(28, 'default', 'updated', 'App\\Models\\BusinessStaffMember', 1, NULL, NULL, '{\"attributes\":{\"id\":1,\"name\":\"Udit Arora\",\"email\":\"uarora1997@gmail.com\",\"password\":\"$2y$10$\\/X0zFrDZp.F9K5EPWJ657.oHqnrWjQ4MNM8sMCfWhrN7OCzQuEYMi\",\"status\":\"active\",\"zoom_active\":\"\",\"zoom_refresh\":\"\",\"paypal_token\":\"\",\"stripe_token\":\"\",\"code\":\"szq3kWrcV8\",\"timezone\":\"Africa\\/Abidjan\",\"language\":\"German\",\"date_format\":\"MM\\/DD\\/YYYY\",\"time_format\":\"24h\",\"business_id\":1,\"created_at\":\"2021-07-04T17:30:46.000000Z\",\"updated_at\":\"2021-07-09T05:19:14.000000Z\",\"deleted_at\":null,\"remember_token\":null,\"email_verified_at\":null,\"profile\":null,\"logo\":null,\"description\":\"uarora1997@gmail.com\",\"g_selected_email\":\"uditarora.ubs@gmail.com\",\"g_calendar_id\":null},\"old\":{\"id\":1,\"name\":\"Udit Arora\",\"email\":\"uarora1997@gmail.com\",\"password\":\"$2y$10$\\/X0zFrDZp.F9K5EPWJ657.oHqnrWjQ4MNM8sMCfWhrN7OCzQuEYMi\",\"status\":\"active\",\"zoom_active\":\"\",\"zoom_refresh\":\"\",\"paypal_token\":\"\",\"stripe_token\":\"\",\"code\":\"szq3kWrcV8\",\"timezone\":\"Africa\\/Abidjan\",\"language\":\"German\",\"date_format\":\"MM\\/DD\\/YYYY\",\"time_format\":\"12h (am\\/pm)\",\"business_id\":1,\"created_at\":\"2021-07-04T17:30:46.000000Z\",\"updated_at\":\"2021-07-09T05:18:14.000000Z\",\"deleted_at\":null,\"remember_token\":null,\"email_verified_at\":null,\"profile\":null,\"logo\":null,\"description\":\"uarora1997@gmail.com\",\"g_selected_email\":\"uditarora.ubs@gmail.com\",\"g_calendar_id\":null}}', '2021-07-08 23:49:14', '2021-07-08 23:49:14'),
(29, 'default', 'updated', 'App\\Models\\BusinessStaffMember', 1, NULL, NULL, '{\"attributes\":{\"id\":1,\"name\":\"Udit Arora\",\"email\":\"uarora1997@gmail.com\",\"password\":\"$2y$10$\\/X0zFrDZp.F9K5EPWJ657.oHqnrWjQ4MNM8sMCfWhrN7OCzQuEYMi\",\"status\":\"active\",\"zoom_active\":\"\",\"zoom_refresh\":\"\",\"paypal_token\":\"\",\"stripe_token\":\"\",\"code\":\"szq3kWrcV8\",\"timezone\":\"America\\/Argentina\\/Rio_Gallegos\",\"language\":\"Spanish\",\"date_format\":\"DD\\/MM\\/YYYY\",\"time_format\":\"12h (am\\/pm)\",\"business_id\":1,\"created_at\":\"2021-07-04T17:30:46.000000Z\",\"updated_at\":\"2021-07-09T05:19:36.000000Z\",\"deleted_at\":null,\"remember_token\":null,\"email_verified_at\":null,\"profile\":null,\"logo\":null,\"description\":\"uarora1997@gmail.com\",\"g_selected_email\":\"uditarora.ubs@gmail.com\",\"g_calendar_id\":null},\"old\":{\"id\":1,\"name\":\"Udit Arora\",\"email\":\"uarora1997@gmail.com\",\"password\":\"$2y$10$\\/X0zFrDZp.F9K5EPWJ657.oHqnrWjQ4MNM8sMCfWhrN7OCzQuEYMi\",\"status\":\"active\",\"zoom_active\":\"\",\"zoom_refresh\":\"\",\"paypal_token\":\"\",\"stripe_token\":\"\",\"code\":\"szq3kWrcV8\",\"timezone\":\"Africa\\/Abidjan\",\"language\":\"German\",\"date_format\":\"MM\\/DD\\/YYYY\",\"time_format\":\"24h\",\"business_id\":1,\"created_at\":\"2021-07-04T17:30:46.000000Z\",\"updated_at\":\"2021-07-09T05:19:14.000000Z\",\"deleted_at\":null,\"remember_token\":null,\"email_verified_at\":null,\"profile\":null,\"logo\":null,\"description\":\"uarora1997@gmail.com\",\"g_selected_email\":\"uditarora.ubs@gmail.com\",\"g_calendar_id\":null}}', '2021-07-08 23:49:36', '2021-07-08 23:49:36'),
(30, 'default', 'updated', 'App\\Models\\BusinessStaffMember', 1, NULL, NULL, '{\"attributes\":{\"id\":1,\"name\":\"Udit Arora\",\"email\":\"uarora1@gmail.com\",\"password\":\"$2y$10$\\/X0zFrDZp.F9K5EPWJ657.oHqnrWjQ4MNM8sMCfWhrN7OCzQuEYMi\",\"status\":\"active\",\"zoom_active\":\"\",\"zoom_refresh\":\"\",\"paypal_token\":\"\",\"stripe_token\":\"\",\"code\":\"szq3kWrcV8\",\"timezone\":\"America\\/Argentina\\/Rio_Gallegos\",\"language\":\"Spanish\",\"date_format\":\"DD\\/MM\\/YYYY\",\"time_format\":\"12h (am\\/pm)\",\"business_id\":1,\"created_at\":\"2021-07-04T17:30:46.000000Z\",\"updated_at\":\"2021-07-09T05:32:47.000000Z\",\"deleted_at\":null,\"remember_token\":null,\"email_verified_at\":null,\"profile\":null,\"logo\":null,\"description\":\"uarora1997@gmail.com\",\"g_selected_email\":\"uditarora.ubs@gmail.com\",\"g_calendar_id\":null},\"old\":{\"id\":1,\"name\":\"Udit Arora\",\"email\":\"uarora1997@gmail.com\",\"password\":\"$2y$10$\\/X0zFrDZp.F9K5EPWJ657.oHqnrWjQ4MNM8sMCfWhrN7OCzQuEYMi\",\"status\":\"active\",\"zoom_active\":\"\",\"zoom_refresh\":\"\",\"paypal_token\":\"\",\"stripe_token\":\"\",\"code\":\"szq3kWrcV8\",\"timezone\":\"America\\/Argentina\\/Rio_Gallegos\",\"language\":\"Spanish\",\"date_format\":\"DD\\/MM\\/YYYY\",\"time_format\":\"12h (am\\/pm)\",\"business_id\":1,\"created_at\":\"2021-07-04T17:30:46.000000Z\",\"updated_at\":\"2021-07-09T05:19:36.000000Z\",\"deleted_at\":null,\"remember_token\":null,\"email_verified_at\":null,\"profile\":null,\"logo\":null,\"description\":\"uarora1997@gmail.com\",\"g_selected_email\":\"uditarora.ubs@gmail.com\",\"g_calendar_id\":null}}', '2021-07-09 00:02:47', '2021-07-09 00:02:47'),
(31, 'default', 'updated', 'App\\Models\\StaffSchedule', 2, NULL, NULL, '{\"attributes\":{\"id\":2,\"name\":\"kuldeeptest\",\"staff_id\":1,\"is_default\":\"no\",\"created_at\":\"2021-07-06T16:18:38.000000Z\",\"updated_at\":\"2021-07-12T20:15:12.000000Z\",\"monday\":\"{\\\"start_time\\\":[\\\"09:00\\\",\\\"15:00\\\"],\\\"end_time\\\":[\\\"13:00\\\",\\\"17:00\\\"]}\",\"tuesday\":\"{\\\"start_time\\\":[\\\"12:00\\\"],\\\"end_time\\\":[\\\"14:00\\\"]}\",\"wednesday\":null,\"thursday\":null,\"friday\":null,\"saturday\":\"{\\\"start_time\\\":[\\\"12:00\\\"],\\\"end_time\\\":[\\\"15:00\\\"]}\",\"sunday\":null},\"old\":{\"id\":2,\"name\":\"kuldeeptest\",\"staff_id\":1,\"is_default\":\"no\",\"created_at\":\"2021-07-06T16:18:38.000000Z\",\"updated_at\":\"2021-07-06T16:23:24.000000Z\",\"monday\":\"{\\\"start_time\\\":[\\\"18:00\\\"],\\\"end_time\\\":[\\\"19:00\\\"]}\",\"tuesday\":\"{\\\"start_time\\\":[\\\"12:00\\\"],\\\"end_time\\\":[\\\"14:00\\\"]}\",\"wednesday\":null,\"thursday\":null,\"friday\":null,\"saturday\":\"{\\\"start_time\\\":[\\\"12:00\\\"],\\\"end_time\\\":[\\\"15:00\\\"]}\",\"sunday\":null}}', '2021-07-12 14:45:12', '2021-07-12 14:45:12'),
(32, 'default', 'updated', 'App\\Models\\StaffSchedule', 1, NULL, NULL, '{\"attributes\":{\"id\":1,\"name\":\"adsf\",\"staff_id\":1,\"is_default\":\"no\",\"created_at\":\"2021-07-04T17:31:44.000000Z\",\"updated_at\":\"2021-07-12T20:17:28.000000Z\",\"monday\":\"{\\\"start_time\\\":[\\\"9:00\\\"],\\\"end_time\\\":[\\\"18:00\\\"]}\",\"tuesday\":null,\"wednesday\":null,\"thursday\":null,\"friday\":null,\"saturday\":null,\"sunday\":null},\"old\":{\"id\":1,\"name\":\"adsf\",\"staff_id\":1,\"is_default\":\"no\",\"created_at\":\"2021-07-04T17:31:44.000000Z\",\"updated_at\":\"2021-07-04T17:31:44.000000Z\",\"monday\":\"{\\\"start_time\\\":[\\\"9:00\\\"],\\\"end_time\\\":[\\\"10:00\\\"]}\",\"tuesday\":null,\"wednesday\":null,\"thursday\":null,\"friday\":null,\"saturday\":null,\"sunday\":null}}', '2021-07-12 14:47:28', '2021-07-12 14:47:28'),
(33, 'default', 'updated', 'App\\Models\\StaffSchedule', 1, NULL, NULL, '{\"attributes\":{\"id\":1,\"name\":\"adsf\",\"staff_id\":1,\"is_default\":\"no\",\"created_at\":\"2021-07-04T17:31:44.000000Z\",\"updated_at\":\"2021-07-12T20:17:45.000000Z\",\"monday\":\"{\\\"start_time\\\":[\\\"9:00\\\",\\\"19:00\\\"],\\\"end_time\\\":[\\\"18:00\\\",\\\"21:00\\\"]}\",\"tuesday\":null,\"wednesday\":null,\"thursday\":null,\"friday\":null,\"saturday\":null,\"sunday\":null},\"old\":{\"id\":1,\"name\":\"adsf\",\"staff_id\":1,\"is_default\":\"no\",\"created_at\":\"2021-07-04T17:31:44.000000Z\",\"updated_at\":\"2021-07-12T20:17:28.000000Z\",\"monday\":\"{\\\"start_time\\\":[\\\"9:00\\\"],\\\"end_time\\\":[\\\"18:00\\\"]}\",\"tuesday\":null,\"wednesday\":null,\"thursday\":null,\"friday\":null,\"saturday\":null,\"sunday\":null}}', '2021-07-12 14:47:45', '2021-07-12 14:47:45');

-- --------------------------------------------------------

--
-- Table structure for table `bookings`
--

CREATE TABLE `bookings` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `staff_serviceid` int(11) NOT NULL,
  `staff_id` int(11) NOT NULL,
  `status` enum('Cancelled','Rescheduled','Active') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Active',
  `slot` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `duration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meeting_location` enum('zoom','googlemeet','custom') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'custom',
  `meeting_link` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `currency` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'USD',
  `date` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `amount` decimal(9,3) NOT NULL DEFAULT 0.000,
  `buffer_time` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `meeting_notes` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `cancelres_notes` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `meeting_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'onetoonemeet',
  `google_eventID` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `current_participants` int(11) NOT NULL DEFAULT 0,
  `max_participants` int(11) NOT NULL DEFAULT 0,
  `booking_ids` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `bookings`
--

INSERT INTO `bookings` (`id`, `staff_serviceid`, `staff_id`, `status`, `slot`, `duration`, `meeting_location`, `meeting_link`, `currency`, `date`, `amount`, `buffer_time`, `meeting_notes`, `cancelres_notes`, `meeting_type`, `google_eventID`, `current_participants`, `max_participants`, `booking_ids`, `created_at`, `updated_at`) VALUES
(1, 2, 1, 'Active', '03:30:00', '0 hours30 minutes', 'custom', '', 'USD', '2021-07-19', '0.000', '0:0', '', '', 'onetoonemeet', '', 1, 1, '1', '2021-07-13 18:16:06', '2021-07-13 18:16:06'),
(2, 3, 1, 'Active', '04:30:00', '0 hours30 minutes', 'custom', '', 'USD', '2021-07-19', '0.000', '0:0', '', '', 'onetoonemeet', '', 1, 1, '2', '2021-07-13 18:18:28', '2021-07-13 18:18:28'),
(3, 4, 1, 'Active', '03:30:00', '0 hours30 minutes', 'custom', '', 'USD', '2021-07-19', '0.000', '1:30', '', '', 'onetoonemeet', '', 1, 1, '3', '2021-07-13 18:34:51', '2021-07-13 18:34:52');

-- --------------------------------------------------------

--
-- Table structure for table `booking_formvalues`
--

CREATE TABLE `booking_formvalues` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `bookingid` int(11) NOT NULL,
  `staff_serviceid` int(11) NOT NULL,
  `staffid` int(11) NOT NULL,
  `user_timezone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `paymode` enum('Stripe','Paypal','Offline') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Offline',
  `transaction_id` int(11) NOT NULL DEFAULT 0,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `field_value_1` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `field_value_2` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `field_value_3` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `field_value_4` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `field_value_5` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `guest_email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cancelres_notes` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `cancelres_by` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `status` enum('Cancelled','Rescheduled','Active') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `booking_formvalues`
--

INSERT INTO `booking_formvalues` (`id`, `bookingid`, `staff_serviceid`, `staffid`, `user_timezone`, `paymode`, `transaction_id`, `name`, `email`, `field_value_1`, `field_value_2`, `field_value_3`, `field_value_4`, `field_value_5`, `guest_email`, `cancelres_notes`, `cancelres_by`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 2, 1, 'GMT+0530 (India Standard Time)', 'Offline', 0, 'Udit Arora', 'uarora1997@gmail.com', '[{\"Message\":\"sac\"},{\"Message\":\"as\"},{\"Message\":\"das\"}]', '[]', '[]', '[]', '[]', 'uarora1997@gmail.com', '', '', 'Active', '2021-07-13 18:16:06', '2021-07-13 18:16:06'),
(2, 2, 3, 1, 'GMT+0530 (India Standard Time)', 'Offline', 0, 'Udit Arora', 'uarora1@gmail.com', '[{\"Message\":\"ads\"},{\"Message\":\"asd\"},{\"Message\":\"ad\"}]', '[]', '[]', '[]', '[]', 'uarora1@gmail.com', '', '', 'Active', '2021-07-13 18:18:28', '2021-07-13 18:18:28'),
(3, 3, 4, 1, 'GMT+0530 (India Standard Time)', 'Offline', 0, 'Udit Arora', 'uarora1997@gmail.com', '[{\"Message\":null},{\"Message\":null}]', '[]', '[]', '[]', '[]', 'udit@probooking.io', '', '', 'Active', '2021-07-13 18:34:51', '2021-07-13 18:34:51');

-- --------------------------------------------------------

--
-- Table structure for table `brandcolors`
--

CREATE TABLE `brandcolors` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `brand_color` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `staff_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `logo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `brandcolors`
--

INSERT INTO `brandcolors` (`id`, `brand_color`, `staff_id`, `logo`, `created_at`, `updated_at`) VALUES
(1, '#ff6161', '1', '1625982236.png', '2021-07-11 00:13:57', '2021-07-11 00:13:57');

-- --------------------------------------------------------

--
-- Table structure for table `businesses`
--

CREATE TABLE `businesses` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `business_owner_id` bigint(20) UNSIGNED NOT NULL,
  `logo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `timezone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'UTC',
  `code` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('active','disabled') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `language` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `businesses`
--

INSERT INTO `businesses` (`id`, `name`, `business_owner_id`, `logo`, `timezone`, `code`, `status`, `created_at`, `updated_at`, `deleted_at`, `language`, `description`) VALUES
(1, 'sfd', 2, NULL, 'Africa/Bujumbura', 'MQB6ZjWm2X', 'active', '2021-07-04 11:59:39', '2021-07-04 11:59:39', NULL, 'Italian', 'sdf');

-- --------------------------------------------------------

--
-- Table structure for table `business_locations`
--

CREATE TABLE `business_locations` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `business_id` bigint(20) UNSIGNED NOT NULL,
  `address` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `latitude` decimal(8,2) DEFAULT NULL,
  `longitude` decimal(8,2) DEFAULT NULL,
  `phone` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `code` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('active','disabled') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `business_owners`
--

CREATE TABLE `business_owners` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('active','disabled') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'active',
  `timezone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'UTC',
  `code` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `language` enum('es','en') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'en'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `business_owners`
--

INSERT INTO `business_owners` (`id`, `name`, `email`, `password`, `status`, `timezone`, `code`, `remember_token`, `email_verified_at`, `created_at`, `updated_at`, `deleted_at`, `language`) VALUES
(1, 'Ms. Laila Rodriguez DDS', 'torrey.weber@example.net', '$2y$10$9uCw9hr00P9cuPr2/jMASuJUb7SQHx.hxRpAJRq1jFYV0czELHAya', 'active', 'UTC', 'jTRDTvvy6E', 'tBygeDCtKn', '2021-07-04 11:53:25', '2021-07-04 11:53:25', '2021-07-04 11:53:25', NULL, 'en'),
(2, 'udit', 'owner@gmail.com', '$2y$12$BbW3T0iF74K9IAjk5KOffef7dDQoo2oQUsemtpqZhnqXznUupoAf.', 'active', 'UTC', '', NULL, NULL, NULL, NULL, NULL, 'en');

-- --------------------------------------------------------

--
-- Table structure for table `business_plans`
--

CREATE TABLE `business_plans` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('active','disabled') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'active',
  `can_add_staff_members` enum('yes','no') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'yes',
  `code` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `staff_member_limit` smallint(6) NOT NULL DEFAULT 1,
  `location_limit` smallint(6) NOT NULL DEFAULT 1,
  `categories_limit` smallint(6) NOT NULL DEFAULT 1,
  `services_limit` smallint(6) NOT NULL DEFAULT 1,
  `signup_form_limit` smallint(6) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `business_schedules`
--

CREATE TABLE `business_schedules` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `business_id` bigint(20) UNSIGNED NOT NULL,
  `is_default` enum('yes','no') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'no',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `monday` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tuesday` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `wednesday` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `thursday` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `friday` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `saturday` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sunday` text COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `business_services`
--

CREATE TABLE `business_services` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('active','disabled') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'active',
  `business_id` bigint(20) UNSIGNED NOT NULL,
  `code` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `price` decimal(8,2) NOT NULL DEFAULT 0.00,
  `currency` varchar(3) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `signup_form_id` bigint(20) UNSIGNED DEFAULT NULL,
  `round_robin` enum('yes','no') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'no',
  `service_category_id` bigint(20) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `business_service_categories`
--

CREATE TABLE `business_service_categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `business_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('active','disabled') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `business_service_signup_forms`
--

CREATE TABLE `business_service_signup_forms` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `business_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('active','disabled') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `business_service_signup_form_fields`
--

CREATE TABLE `business_service_signup_form_fields` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `signup_form_id` bigint(20) UNSIGNED NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `is_required` enum('yes','no') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'yes',
  `options` text COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `business_service_staff_members`
--

CREATE TABLE `business_service_staff_members` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `business_service_id` bigint(20) UNSIGNED NOT NULL,
  `business_staff_member_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `business_staff_members`
--

CREATE TABLE `business_staff_members` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('active','disabled') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'active',
  `zoom_active` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `zoom_refresh` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `paypal_token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `stripe_token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `code` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `timezone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'UTC',
  `language` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `date_format` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `time_format` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `business_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `profile` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `logo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `g_selected_email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `g_calendar_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `business_staff_members`
--

INSERT INTO `business_staff_members` (`id`, `name`, `email`, `password`, `status`, `zoom_active`, `zoom_refresh`, `paypal_token`, `stripe_token`, `code`, `timezone`, `language`, `date_format`, `time_format`, `business_id`, `created_at`, `updated_at`, `deleted_at`, `remember_token`, `email_verified_at`, `profile`, `logo`, `description`, `g_selected_email`, `g_calendar_id`) VALUES
(1, 'Udit Arora', 'uarora1997@gmail.com', '$2y$10$/X0zFrDZp.F9K5EPWJ657.oHqnrWjQ4MNM8sMCfWhrN7OCzQuEYMi', 'active', '', '', '', '', 'szq3kWrcV8', 'America/Argentina/Rio_Gallegos', 'Spanish', 'DD/MM/YYYY', '12h (am/pm)', 1, '2021-07-04 12:00:46', '2021-07-09 00:02:47', NULL, NULL, NULL, NULL, NULL, 'uarora1997@gmail.com', 'uditarora.ubs@gmail.com', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `g_calendars`
--

CREATE TABLE `g_calendars` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `staff_id` bigint(20) UNSIGNED NOT NULL,
  `access_token` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `refresh_token` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `calendar_block` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `account` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `jobs`
--

CREATE TABLE `jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `queue` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `attempts` tinyint(3) UNSIGNED NOT NULL,
  `reserved_at` int(10) UNSIGNED DEFAULT NULL,
  `available_at` int(10) UNSIGNED NOT NULL,
  `created_at` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2021_01_29_160425_create_timezones_table', 1),
(5, '2021_01_29_172339_create_activity_log_table', 1),
(6, '2021_01_29_184048_create_business_owners_table', 1),
(7, '2021_02_03_221634_create_businesses_table', 1),
(8, '2021_02_04_150949_create_business_locations_table', 1),
(9, '2021_02_10_001824_create_business_staff_members_table', 1),
(10, '2021_02_15_225339_create_permission_tables', 1),
(11, '2021_02_18_010138_add_language_column_to_users_table', 1),
(12, '2021_02_21_224036_create_business_plans_table', 1),
(13, '2021_02_24_235320_add_language_column_to_businesses_table', 1),
(14, '2021_02_25_005557_add_language_column_to_business_owners_table', 1),
(15, '2021_02_26_232922_create_business_services_table', 1),
(16, '2021_03_25_010718_add_description_to_businesses_table', 1),
(17, '2021_03_27_163638_add_price_column_to_business_service_table', 1),
(18, '2021_03_28_004239_create_business_service_staff_members_table', 1),
(19, '2021_03_28_183129_add_image_column_to_business_services_table', 1),
(20, '2021_03_28_185901_create_business_service_signup_forms_table', 1),
(21, '2021_03_28_205146_create_business_service_signup_form_fields_table', 1),
(22, '2021_03_29_014337_add_required_column_to_business_service_signup_form_fields_table', 1),
(23, '2021_03_29_125516_add_signup_form_id_column_to_business_services_table', 1),
(24, '2021_03_29_203552_add_options_column_to_business_service_signup_form_fields_table', 1),
(25, '2021_03_29_225414_add_round_robin_column_to_business_services_table', 1),
(26, '2021_03_31_195032_create_business_service_categories_table', 1),
(27, '2021_03_31_233418_add_service_category_id_column_to_business_service_table', 1),
(28, '2021_04_03_015038_add_new_columns_to_business_plans_table', 1),
(29, '2021_04_08_221140_create_business_schedules_table', 1),
(30, '2021_05_06_100839_change_business_schedule_days_table', 1),
(31, '2021_05_09_095414_create_staff_schedules_table', 1),
(33, '2021_05_13_164404_create_brandcolors_table', 1),
(34, '2021_05_13_170018_create_sidebarlogos_table', 1),
(35, '2021_05_15_150152_create_side_barcolor_businesses_table', 1),
(36, '2021_05_15_155543_create_side_logo_businesses_table', 1),
(37, '2021_05_16_094345_add_logo_to_brandcolor_table', 1),
(38, '2021_05_17_154800_add_logo_to_side_barcolor_businesses_table', 1),
(39, '2021_05_17_155329_add_columns_to_business_staff_members_table', 1),
(40, '2021_05_18_155043_create_accountsetting_staffs_table', 1),
(41, '2021_05_28_123855_change_staff_schedule_table', 1),
(42, '2021_05_29_160036_change_business_schedule_table', 1),
(43, '2021_06_04_015144_create_staff_booking_formfields', 1),
(46, '2021_06_29_140617_create_g_calendars_table', 1),
(47, '2021_06_29_142256_add_column_to_business_staff_members_table', 1),
(48, '2021_07_06_022432_create_jobs_table', 2),
(51, '2021_05_12_141013_create_staffservices_table', 4),
(53, '2021_06_07_030857_create_booking_formvalues', 5),
(54, '2021_06_07_030556_create_bookings', 6);

-- --------------------------------------------------------

--
-- Table structure for table `model_has_permissions`
--

CREATE TABLE `model_has_permissions` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `model_has_roles`
--

CREATE TABLE `model_has_roles` (
  `role_id` bigint(20) UNSIGNED NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `model_has_roles`
--

INSERT INTO `model_has_roles` (`role_id`, `model_type`, `model_id`) VALUES
(1, 'App\\Models\\User', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `group` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `group`, `guard_name`, `created_at`, `updated_at`) VALUES
(1, 'user-list', 'user', 'web', '2021-07-04 11:53:19', '2021-07-04 11:53:19'),
(2, 'user-create', 'user', 'web', '2021-07-04 11:53:19', '2021-07-04 11:53:19'),
(3, 'user-edit', 'user', 'web', '2021-07-04 11:53:20', '2021-07-04 11:53:20'),
(4, 'user-delete', 'user', 'web', '2021-07-04 11:53:20', '2021-07-04 11:53:20'),
(5, 'user-role-list', 'user-role', 'web', '2021-07-04 11:53:20', '2021-07-04 11:53:20'),
(6, 'user-role-create', 'user-role', 'web', '2021-07-04 11:53:20', '2021-07-04 11:53:20'),
(7, 'user-role-edit', 'user-role', 'web', '2021-07-04 11:53:20', '2021-07-04 11:53:20'),
(8, 'user-role-delete', 'user-role', 'web', '2021-07-04 11:53:20', '2021-07-04 11:53:20'),
(9, 'business-list', 'business', 'web', '2021-07-04 11:53:20', '2021-07-04 11:53:20'),
(10, 'business-create', 'business', 'web', '2021-07-04 11:53:20', '2021-07-04 11:53:20'),
(11, 'business-edit', 'business', 'web', '2021-07-04 11:53:20', '2021-07-04 11:53:20'),
(12, 'business-delete', 'business', 'web', '2021-07-04 11:53:20', '2021-07-04 11:53:20'),
(13, 'business-owner-list', 'business', 'web', '2021-07-04 11:53:20', '2021-07-04 11:53:20'),
(14, 'business-owner-create', 'business', 'web', '2021-07-04 11:53:20', '2021-07-04 11:53:20'),
(15, 'business-owner-edit', 'business', 'web', '2021-07-04 11:53:20', '2021-07-04 11:53:20'),
(16, 'business-owner-delete', 'business', 'web', '2021-07-04 11:53:20', '2021-07-04 11:53:20'),
(17, 'business-plan-list', 'business', 'web', '2021-07-04 11:53:20', '2021-07-04 11:53:20'),
(18, 'business-plan-create', 'business', 'web', '2021-07-04 11:53:20', '2021-07-04 11:53:20'),
(19, 'business-plan-edit', 'business', 'web', '2021-07-04 11:53:20', '2021-07-04 11:53:20'),
(20, 'business-plan-delete', 'business', 'web', '2021-07-04 11:53:20', '2021-07-04 11:53:20');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `show_on_list` enum('yes','no') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'yes',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `guard_name`, `show_on_list`, `created_at`, `updated_at`) VALUES
(1, 'Super Admin', 'web', 'no', '2021-07-04 11:53:21', '2021-07-04 11:53:21');

-- --------------------------------------------------------

--
-- Table structure for table `role_has_permissions`
--

CREATE TABLE `role_has_permissions` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `sidebarlogos`
--

CREATE TABLE `sidebarlogos` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `logo` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `staff_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `side_barcolor_businesses`
--

CREATE TABLE `side_barcolor_businesses` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `business_side_b_color` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `logo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `business_d` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `side_barcolor_businesses`
--

INSERT INTO `side_barcolor_businesses` (`id`, `business_side_b_color`, `logo`, `business_d`, `created_at`, `updated_at`) VALUES
(1, '#ff6161', '1625982236.png', '1', '2021-07-11 00:13:57', '2021-07-11 00:13:57');

-- --------------------------------------------------------

--
-- Table structure for table `side_logo_businesses`
--

CREATE TABLE `side_logo_businesses` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `logo` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `staffservices`
--

CREATE TABLE `staffservices` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `status` enum('active','disabled','draft') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'draft',
  `staff_id` bigint(20) UNSIGNED NOT NULL,
  `paidappointment` tinyint(4) NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `price` decimal(8,2) NOT NULL DEFAULT 0.00,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `interval` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `duration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `schedule_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meeting_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'onetoonemeet',
  `participants_count` int(11) NOT NULL DEFAULT 0,
  `meetings_count_perday` int(11) NOT NULL DEFAULT 0,
  `notice_period` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `available_till` int(11) NOT NULL DEFAULT 0,
  `location` enum('zoom','meet','custom') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'custom',
  `paymode` enum('Offline','Stripe','Paypal') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Offline',
  `buffer_time` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `redirectURL` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cancelresc` tinyint(4) NOT NULL,
  `confirm_subject` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `confirm_body` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `cancel_subject` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `cancel_body` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `reschedule_subject` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `reschedule_body` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `followup_subject` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `followup_body` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `reminder_subject` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `reminder_body` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `reminder_timing` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `followup_timing` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `location_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `is_deleted` tinyint(4) NOT NULL DEFAULT 0,
  `is_paused` tinyint(4) NOT NULL DEFAULT 0,
  `flag_bookingform` tinyint(4) NOT NULL DEFAULT 0,
  `reminder` tinyint(4) NOT NULL DEFAULT 0,
  `followup` tinyint(4) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `staffservices`
--

INSERT INTO `staffservices` (`id`, `status`, `staff_id`, `paidappointment`, `image`, `price`, `name`, `interval`, `duration`, `description`, `schedule_id`, `meeting_type`, `participants_count`, `meetings_count_perday`, `notice_period`, `available_till`, `location`, `paymode`, `buffer_time`, `redirectURL`, `cancelresc`, `confirm_subject`, `confirm_body`, `cancel_subject`, `cancel_body`, `reschedule_subject`, `reschedule_body`, `followup_subject`, `followup_body`, `reminder_subject`, `reminder_body`, `reminder_timing`, `followup_timing`, `location_name`, `is_deleted`, `is_paused`, `flag_bookingform`, `reminder`, `followup`, `created_at`, `updated_at`) VALUES
(1, 'active', 1, 1, '', '0.00', 'Udit Arora', '30', '0:30', 'asas', '1', 'onetoonemeet', 1, 10, '10 mins', 10, 'custom', 'Offline', '0:0', NULL, 0, 'Hi {{name}}', '<p>Your {{duration}} meeting for {{name}} slot {{slot}} is confirmed. Location : {{meeting_link}}</p>', 'Booking Cancellation', '<p>Your booking for {{name}} - slot {{slot}} is cancelled.</p>', 'Booking Rescheduled', '<p>Your {{duration}} meeting for {{name}} slot {{slot}} is rescheduled.</p>', 'Thank you for your time!', '<p>Hi {{name}}, Thank you for attending {{Event_Name}} at {{Event_Time}} on {{Event_Date}}. Please respond to this email with any feedback or additional requests.</p>', 'Reminder:{{Event_Name}} with {{My_Name}} at {{Event_Time}} on {{Event_Date}}', '<p>Hi {{name}}, This is a friendly reminder that your {{Event_Name}} with {{My_Name}} is at {{Event_Time}} on {{Event_Date}} {{location}} {{event_description}} {{booking_form}}</p>', '', '', '', 1, 0, 1, 0, 0, '2021-07-13 18:07:52', '2021-07-13 18:11:32'),
(2, 'active', 1, 1, '', '0.00', 'asdasd', '30', '0:30', 'asdasas', '1', 'onetoonemeet', 1, 10, '10 mins', 10, 'custom', 'Offline', '0:0', NULL, 0, 'Hi {{name}}', '<p>Your {{duration}} meeting for {{name}} slot {{slot}} is confirmed. Location : {{meeting_link}}</p>', 'Booking Cancellation', '<p>Your booking for {{name}} - slot {{slot}} is cancelled.</p>', 'Booking Rescheduled', '<p>Your {{duration}} meeting for {{name}} slot {{slot}} is rescheduled.</p>', 'Thank you for your time!', '<p>Hi {{name}}, Thank you for attending {{Event_Name}} at {{Event_Time}} on {{Event_Date}}. Please respond to this email with any feedback or additional requests.</p>', 'Reminder:{{Event_Name}} with {{My_Name}} at {{Event_Time}} on {{Event_Date}}', '<p>Hi {{name}}, This is a friendly reminder that your {{Event_Name}} with {{My_Name}} is at {{Event_Time}} on {{Event_Date}} {{location}} {{event_description}} {{booking_form}}</p>', '', '', '', 0, 0, 1, 0, 0, '2021-07-13 18:11:54', '2021-07-13 18:12:04'),
(3, 'active', 1, 1, '', '0.00', 'resc', '30', '0:30', 'adsadsads', '1', 'onetoonemeet', 1, 10, '10 mins', 10, 'custom', 'Offline', '0:0', NULL, 1, 'Hi {{name}}', '<p>Your {{duration}} meeting for {{name}} slot {{slot}} is confirmed. Location : {{meeting_link}}</p>', 'Booking Cancellation', '<p>Your booking for {{name}} - slot {{slot}} is cancelled.</p>', 'Booking Rescheduled', '<p>Your {{duration}} meeting for {{name}} slot {{slot}} is rescheduled.</p>', 'Thank you for your time!', '<p>Hi {{name}}, Thank you for attending {{Event_Name}} at {{Event_Time}} on {{Event_Date}}. Please respond to this email with any feedback or additional requests.</p>', 'Reminder:{{Event_Name}} with {{My_Name}} at {{Event_Time}} on {{Event_Date}}', '<p>Hi {{name}}, This is a friendly reminder that your {{Event_Name}} with {{My_Name}} is at {{Event_Time}} on {{Event_Date}} {{location}} {{event_description}} {{booking_form}}</p>', '', '', '', 0, 0, 1, 0, 0, '2021-07-13 18:17:35', '2021-07-13 18:17:40'),
(4, 'active', 1, 1, '', '0.00', 'asdsadads', '30', '0:30', 'adsdasd', '1', 'onetoonemeet', 1, 10, '10 mins', 10, 'custom', 'Offline', '1:30', NULL, 0, 'Hi {{name}}', '<p>Your {{duration}} meeting for {{name}} slot {{slot}} is confirmed. Location : {{meeting_link}}</p>', 'Booking Cancellation', '<p>Your booking for {{name}} - slot {{slot}} is cancelled.</p>', 'Booking Rescheduled', '<p>Your {{duration}} meeting for {{name}} slot {{slot}} is rescheduled.</p>', 'Thank you for your time!', '<p>Hi {{name}}, Thank you for attending {{Event_Name}} at {{Event_Time}} on {{Event_Date}}. Please respond to this email with any feedback or additional requests.</p>', 'Reminder:{{Event_Name}} with {{My_Name}} at {{Event_Time}} on {{Event_Date}}', '<p>Hi {{name}}, This is a friendly reminder that your {{Event_Name}} with {{My_Name}} is at {{Event_Time}} on {{Event_Date}} {{location}} {{event_description}} {{booking_form}}</p>', '', '', '', 0, 0, 1, 0, 0, '2021-07-13 18:33:47', '2021-07-13 18:34:06');

-- --------------------------------------------------------

--
-- Table structure for table `staff_booking_formfields`
--

CREATE TABLE `staff_booking_formfields` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `staff_serviceid` int(11) NOT NULL,
  `field_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `field_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `options` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `isrequired` tinyint(4) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `staff_booking_formfields`
--

INSERT INTO `staff_booking_formfields` (`id`, `staff_serviceid`, `field_name`, `field_type`, `options`, `isrequired`, `created_at`, `updated_at`) VALUES
(2, 2, 'Message', 'textarea', '', 1, '2021-07-05 06:13:23', '2021-07-05 06:13:23'),
(3, 3, 'Message', 'textarea', '', 1, '2021-07-06 11:27:30', '2021-07-06 11:27:30'),
(4, 4, 'Message', 'textarea', '', 1, '2021-07-07 03:09:45', '2021-07-07 03:09:45'),
(5, 6, 'Message', 'textarea', '', 1, '2021-07-07 12:41:27', '2021-07-07 12:41:27'),
(6, 6, 'Message', 'textarea', '', 1, '2021-07-07 12:41:41', '2021-07-07 12:41:41'),
(7, 1, 'Message', 'textarea', '', 1, '2021-07-08 07:57:13', '2021-07-08 07:57:13'),
(8, 2, 'Message', 'textarea', '', 1, '2021-07-08 12:04:57', '2021-07-08 12:04:57'),
(9, 6, 'Message', 'textarea', '', 1, '2021-07-10 03:47:03', '2021-07-10 03:47:03'),
(10, 7, 'Message', 'textarea', '', 1, '2021-07-10 03:54:53', '2021-07-10 03:54:53'),
(11, 9, 'Message', 'textarea', '', 1, '2021-07-11 12:40:59', '2021-07-11 12:40:59'),
(12, 10, 'Message', 'textarea', '', 1, '2021-07-11 12:56:00', '2021-07-11 12:56:00'),
(13, 18, 'Message', 'textarea', '', 1, '2021-07-11 14:08:22', '2021-07-11 14:08:22'),
(41, 54, 'Message', 'textarea', '', 1, '2021-07-13 10:02:44', '2021-07-13 10:02:44'),
(43, 56, 'Message', 'textarea', '', 1, '2021-07-13 10:08:08', '2021-07-13 10:08:08'),
(44, 57, 'Message', 'textarea', '', 1, '2021-07-13 10:43:05', '2021-07-13 10:43:05'),
(45, 58, 'Message', 'textarea', '', 1, '2021-07-13 16:50:18', '2021-07-13 16:50:18'),
(46, 1, 'Message', 'textarea', '', 1, '2021-07-13 18:07:58', '2021-07-13 18:07:58'),
(47, 2, 'Message', 'textarea', '', 1, '2021-07-13 18:11:58', '2021-07-13 18:11:58'),
(48, 3, 'Message', 'textarea', '', 1, '2021-07-13 18:17:38', '2021-07-13 18:17:38'),
(49, 3, 'Message', 'textarea', '', 1, '2021-07-13 18:17:39', '2021-07-13 18:17:39'),
(50, 4, 'Message', 'textarea', '', 1, '2021-07-13 18:34:01', '2021-07-13 18:34:01');

-- --------------------------------------------------------

--
-- Table structure for table `staff_schedules`
--

CREATE TABLE `staff_schedules` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `staff_id` bigint(20) UNSIGNED NOT NULL,
  `is_default` enum('yes','no') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'no',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `monday` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tuesday` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `wednesday` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `thursday` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `friday` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `saturday` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sunday` text COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `staff_schedules`
--

INSERT INTO `staff_schedules` (`id`, `name`, `staff_id`, `is_default`, `created_at`, `updated_at`, `monday`, `tuesday`, `wednesday`, `thursday`, `friday`, `saturday`, `sunday`) VALUES
(1, 'adsf', 1, 'no', '2021-07-04 12:01:44', '2021-07-12 14:47:45', '{\"start_time\":[\"9:00\",\"19:00\"],\"end_time\":[\"18:00\",\"21:00\"]}', NULL, NULL, NULL, NULL, NULL, NULL),
(2, 'kuldeeptest', 1, 'no', '2021-07-06 10:48:38', '2021-07-12 14:45:12', '{\"start_time\":[\"09:00\",\"15:00\"],\"end_time\":[\"13:00\",\"17:00\"]}', '{\"start_time\":[\"12:00\"],\"end_time\":[\"14:00\"]}', NULL, NULL, NULL, '{\"start_time\":[\"12:00\"],\"end_time\":[\"15:00\"]}', NULL),
(3, 'sda', 1, 'no', '2021-07-06 10:53:54', '2021-07-06 10:53:54', '{\"start_time\":[\"12:00\"],\"end_time\":[\"15:00\"]}', NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `timezones`
--

CREATE TABLE `timezones` (
  `region` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `timezone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `timezones`
--

INSERT INTO `timezones` (`region`, `timezone`) VALUES
('Pacific/Midway', '(UTC-11:00) Midway'),
('Pacific/Niue', '(UTC-11:00) Niue'),
('Pacific/Pago_Pago', '(UTC-11:00) Pago Pago'),
('America/Adak', '(UTC-10:00) Adak'),
('Pacific/Honolulu', '(UTC-10:00) Honolulu'),
('Pacific/Johnston', '(UTC-10:00) Johnston'),
('Pacific/Rarotonga', '(UTC-10:00) Rarotonga'),
('Pacific/Tahiti', '(UTC-10:00) Tahiti'),
('Pacific/Marquesas', '(UTC-09:30) Marquesas'),
('America/Anchorage', '(UTC-09:00) Anchorage'),
('Pacific/Gambier', '(UTC-09:00) Gambier'),
('America/Juneau', '(UTC-09:00) Juneau'),
('America/Nome', '(UTC-09:00) Nome'),
('America/Sitka', '(UTC-09:00) Sitka'),
('America/Yakutat', '(UTC-09:00) Yakutat'),
('America/Dawson', '(UTC-08:00) Dawson'),
('America/Los_Angeles', '(UTC-08:00) Los Angeles'),
('America/Metlakatla', '(UTC-08:00) Metlakatla'),
('Pacific/Pitcairn', '(UTC-08:00) Pitcairn'),
('America/Santa_Isabel', '(UTC-08:00) Santa Isabel'),
('America/Tijuana', '(UTC-08:00) Tijuana'),
('America/Vancouver', '(UTC-08:00) Vancouver'),
('America/Whitehorse', '(UTC-08:00) Whitehorse'),
('America/Boise', '(UTC-07:00) Boise'),
('America/Cambridge_Bay', '(UTC-07:00) Cambridge Bay'),
('America/Chihuahua', '(UTC-07:00) Chihuahua'),
('America/Creston', '(UTC-07:00) Creston'),
('America/Dawson_Creek', '(UTC-07:00) Dawson Creek'),
('America/Denver', '(UTC-07:00) Denver'),
('America/Edmonton', '(UTC-07:00) Edmonton'),
('America/Hermosillo', '(UTC-07:00) Hermosillo'),
('America/Inuvik', '(UTC-07:00) Inuvik'),
('America/Mazatlan', '(UTC-07:00) Mazatlan'),
('America/Ojinaga', '(UTC-07:00) Ojinaga'),
('America/Phoenix', '(UTC-07:00) Phoenix'),
('America/Shiprock', '(UTC-07:00) Shiprock'),
('America/Yellowknife', '(UTC-07:00) Yellowknife'),
('America/Bahia_Banderas', '(UTC-06:00) Bahia Banderas'),
('America/Belize', '(UTC-06:00) Belize'),
('America/North_Dakota/Beulah', '(UTC-06:00) Beulah'),
('America/Cancun', '(UTC-06:00) Cancun'),
('America/North_Dakota/Center', '(UTC-06:00) Center'),
('America/Chicago', '(UTC-06:00) Chicago'),
('America/Costa_Rica', '(UTC-06:00) Costa Rica'),
('Pacific/Easter', '(UTC-06:00) Easter'),
('America/El_Salvador', '(UTC-06:00) El Salvador'),
('Pacific/Galapagos', '(UTC-06:00) Galapagos'),
('America/Guatemala', '(UTC-06:00) Guatemala'),
('America/Indiana/Knox', '(UTC-06:00) Knox'),
('America/Managua', '(UTC-06:00) Managua'),
('America/Matamoros', '(UTC-06:00) Matamoros'),
('America/Menominee', '(UTC-06:00) Menominee'),
('America/Merida', '(UTC-06:00) Merida'),
('America/Mexico_City', '(UTC-06:00) Mexico City'),
('America/Monterrey', '(UTC-06:00) Monterrey'),
('America/North_Dakota/New_Salem', '(UTC-06:00) New Salem'),
('America/Rainy_River', '(UTC-06:00) Rainy River'),
('America/Rankin_Inlet', '(UTC-06:00) Rankin Inlet'),
('America/Regina', '(UTC-06:00) Regina'),
('America/Resolute', '(UTC-06:00) Resolute'),
('America/Swift_Current', '(UTC-06:00) Swift Current'),
('America/Tegucigalpa', '(UTC-06:00) Tegucigalpa'),
('America/Indiana/Tell_City', '(UTC-06:00) Tell City'),
('America/Winnipeg', '(UTC-06:00) Winnipeg'),
('America/Atikokan', '(UTC-05:00) Atikokan'),
('America/Bogota', '(UTC-05:00) Bogota'),
('America/Cayman', '(UTC-05:00) Cayman'),
('America/Detroit', '(UTC-05:00) Detroit'),
('America/Grand_Turk', '(UTC-05:00) Grand Turk'),
('America/Guayaquil', '(UTC-05:00) Guayaquil'),
('America/Havana', '(UTC-05:00) Havana'),
('America/Indiana/Indianapolis', '(UTC-05:00) Indianapolis'),
('America/Iqaluit', '(UTC-05:00) Iqaluit'),
('America/Jamaica', '(UTC-05:00) Jamaica'),
('America/Lima', '(UTC-05:00) Lima'),
('America/Kentucky/Louisville', '(UTC-05:00) Louisville'),
('America/Indiana/Marengo', '(UTC-05:00) Marengo'),
('America/Kentucky/Monticello', '(UTC-05:00) Monticello'),
('America/Montreal', '(UTC-05:00) Montreal'),
('America/Nassau', '(UTC-05:00) Nassau'),
('America/New_York', '(UTC-05:00) New York'),
('America/Nipigon', '(UTC-05:00) Nipigon'),
('America/Panama', '(UTC-05:00) Panama'),
('America/Pangnirtung', '(UTC-05:00) Pangnirtung'),
('America/Indiana/Petersburg', '(UTC-05:00) Petersburg'),
('America/Port-au-Prince', '(UTC-05:00) Port-au-Prince'),
('America/Thunder_Bay', '(UTC-05:00) Thunder Bay'),
('America/Toronto', '(UTC-05:00) Toronto'),
('America/Indiana/Vevay', '(UTC-05:00) Vevay'),
('America/Indiana/Vincennes', '(UTC-05:00) Vincennes'),
('America/Indiana/Winamac', '(UTC-05:00) Winamac'),
('America/Caracas', '(UTC-04:30) Caracas'),
('America/Anguilla', '(UTC-04:00) Anguilla'),
('America/Antigua', '(UTC-04:00) Antigua'),
('America/Aruba', '(UTC-04:00) Aruba'),
('America/Asuncion', '(UTC-04:00) Asuncion'),
('America/Barbados', '(UTC-04:00) Barbados'),
('Atlantic/Bermuda', '(UTC-04:00) Bermuda'),
('America/Blanc-Sablon', '(UTC-04:00) Blanc-Sablon'),
('America/Boa_Vista', '(UTC-04:00) Boa Vista'),
('America/Campo_Grande', '(UTC-04:00) Campo Grande'),
('America/Cuiaba', '(UTC-04:00) Cuiaba'),
('America/Curacao', '(UTC-04:00) Curacao'),
('America/Dominica', '(UTC-04:00) Dominica'),
('America/Eirunepe', '(UTC-04:00) Eirunepe'),
('America/Glace_Bay', '(UTC-04:00) Glace Bay'),
('America/Goose_Bay', '(UTC-04:00) Goose Bay'),
('America/Grenada', '(UTC-04:00) Grenada'),
('America/Guadeloupe', '(UTC-04:00) Guadeloupe'),
('America/Guyana', '(UTC-04:00) Guyana'),
('America/Halifax', '(UTC-04:00) Halifax'),
('America/Kralendijk', '(UTC-04:00) Kralendijk'),
('America/La_Paz', '(UTC-04:00) La Paz'),
('America/Lower_Princes', '(UTC-04:00) Lower Princes'),
('America/Manaus', '(UTC-04:00) Manaus'),
('America/Marigot', '(UTC-04:00) Marigot'),
('America/Martinique', '(UTC-04:00) Martinique'),
('America/Moncton', '(UTC-04:00) Moncton'),
('America/Montserrat', '(UTC-04:00) Montserrat'),
('Antarctica/Palmer', '(UTC-04:00) Palmer'),
('America/Port_of_Spain', '(UTC-04:00) Port of Spain'),
('America/Porto_Velho', '(UTC-04:00) Porto Velho'),
('America/Puerto_Rico', '(UTC-04:00) Puerto Rico'),
('America/Rio_Branco', '(UTC-04:00) Rio Branco'),
('America/Santiago', '(UTC-04:00) Santiago'),
('America/Santo_Domingo', '(UTC-04:00) Santo Domingo'),
('America/St_Barthelemy', '(UTC-04:00) St. Barthelemy'),
('America/St_Kitts', '(UTC-04:00) St. Kitts'),
('America/St_Lucia', '(UTC-04:00) St. Lucia'),
('America/St_Thomas', '(UTC-04:00) St. Thomas'),
('America/St_Vincent', '(UTC-04:00) St. Vincent'),
('America/Thule', '(UTC-04:00) Thule'),
('America/Tortola', '(UTC-04:00) Tortola'),
('America/St_Johns', '(UTC-03:30) St. Johns'),
('America/Araguaina', '(UTC-03:00) Araguaina'),
('America/Bahia', '(UTC-03:00) Bahia'),
('America/Belem', '(UTC-03:00) Belem'),
('America/Argentina/Buenos_Aires', '(UTC-03:00) Buenos Aires'),
('America/Argentina/Catamarca', '(UTC-03:00) Catamarca'),
('America/Cayenne', '(UTC-03:00) Cayenne'),
('America/Argentina/Cordoba', '(UTC-03:00) Cordoba'),
('America/Fortaleza', '(UTC-03:00) Fortaleza'),
('America/Godthab', '(UTC-03:00) Godthab'),
('America/Argentina/Jujuy', '(UTC-03:00) Jujuy'),
('America/Argentina/La_Rioja', '(UTC-03:00) La Rioja'),
('America/Maceio', '(UTC-03:00) Maceio'),
('America/Argentina/Mendoza', '(UTC-03:00) Mendoza'),
('America/Miquelon', '(UTC-03:00) Miquelon'),
('America/Montevideo', '(UTC-03:00) Montevideo'),
('America/Paramaribo', '(UTC-03:00) Paramaribo'),
('America/Recife', '(UTC-03:00) Recife'),
('America/Argentina/Rio_Gallegos', '(UTC-03:00) Rio Gallegos'),
('Antarctica/Rothera', '(UTC-03:00) Rothera'),
('America/Argentina/Salta', '(UTC-03:00) Salta'),
('America/Argentina/San_Juan', '(UTC-03:00) San Juan'),
('America/Argentina/San_Luis', '(UTC-03:00) San Luis'),
('America/Santarem', '(UTC-03:00) Santarem'),
('America/Sao_Paulo', '(UTC-03:00) Sao Paulo'),
('Atlantic/Stanley', '(UTC-03:00) Stanley'),
('America/Argentina/Tucuman', '(UTC-03:00) Tucuman'),
('America/Argentina/Ushuaia', '(UTC-03:00) Ushuaia'),
('America/Noronha', '(UTC-02:00) Noronha'),
('Atlantic/South_Georgia', '(UTC-02:00) South Georgia'),
('Atlantic/Azores', '(UTC-01:00) Azores'),
('Atlantic/Cape_Verde', '(UTC-01:00) Cape Verde'),
('America/Scoresbysund', '(UTC-01:00) Scoresbysund'),
('Africa/Abidjan', '(UTC+00:00) Abidjan'),
('Africa/Accra', '(UTC+00:00) Accra'),
('Africa/Bamako', '(UTC+00:00) Bamako'),
('Africa/Banjul', '(UTC+00:00) Banjul'),
('Africa/Bissau', '(UTC+00:00) Bissau'),
('Atlantic/Canary', '(UTC+00:00) Canary'),
('Africa/Casablanca', '(UTC+00:00) Casablanca'),
('Africa/Conakry', '(UTC+00:00) Conakry'),
('Africa/Dakar', '(UTC+00:00) Dakar'),
('America/Danmarkshavn', '(UTC+00:00) Danmarkshavn'),
('Europe/Dublin', '(UTC+00:00) Dublin'),
('Africa/El_Aaiun', '(UTC+00:00) El Aaiun'),
('Atlantic/Faroe', '(UTC+00:00) Faroe'),
('Africa/Freetown', '(UTC+00:00) Freetown'),
('Europe/Guernsey', '(UTC+00:00) Guernsey'),
('Europe/Isle_of_Man', '(UTC+00:00) Isle of Man'),
('Europe/Jersey', '(UTC+00:00) Jersey'),
('Europe/Lisbon', '(UTC+00:00) Lisbon'),
('Africa/Lome', '(UTC+00:00) Lome'),
('Europe/London', '(UTC+00:00) London'),
('Atlantic/Madeira', '(UTC+00:00) Madeira'),
('Africa/Monrovia', '(UTC+00:00) Monrovia'),
('Africa/Nouakchott', '(UTC+00:00) Nouakchott'),
('Africa/Ouagadougou', '(UTC+00:00) Ouagadougou'),
('Atlantic/Reykjavik', '(UTC+00:00) Reykjavik'),
('Africa/Sao_Tome', '(UTC+00:00) Sao Tome'),
('Atlantic/St_Helena', '(UTC+00:00) St. Helena'),
('UTC', '(UTC+00:00) UTC'),
('Africa/Algiers', '(UTC+01:00) Algiers'),
('Europe/Amsterdam', '(UTC+01:00) Amsterdam'),
('Europe/Andorra', '(UTC+01:00) Andorra'),
('Africa/Bangui', '(UTC+01:00) Bangui'),
('Europe/Belgrade', '(UTC+01:00) Belgrade'),
('Europe/Berlin', '(UTC+01:00) Berlin'),
('Europe/Bratislava', '(UTC+01:00) Bratislava'),
('Africa/Brazzaville', '(UTC+01:00) Brazzaville'),
('Europe/Brussels', '(UTC+01:00) Brussels'),
('Europe/Budapest', '(UTC+01:00) Budapest'),
('Europe/Busingen', '(UTC+01:00) Busingen'),
('Africa/Ceuta', '(UTC+01:00) Ceuta'),
('Europe/Copenhagen', '(UTC+01:00) Copenhagen'),
('Africa/Douala', '(UTC+01:00) Douala'),
('Europe/Gibraltar', '(UTC+01:00) Gibraltar'),
('Africa/Kinshasa', '(UTC+01:00) Kinshasa'),
('Africa/Lagos', '(UTC+01:00) Lagos'),
('Africa/Libreville', '(UTC+01:00) Libreville'),
('Europe/Ljubljana', '(UTC+01:00) Ljubljana'),
('Arctic/Longyearbyen', '(UTC+01:00) Longyearbyen'),
('Africa/Luanda', '(UTC+01:00) Luanda'),
('Europe/Luxembourg', '(UTC+01:00) Luxembourg'),
('Europe/Madrid', '(UTC+01:00) Madrid'),
('Africa/Malabo', '(UTC+01:00) Malabo'),
('Europe/Malta', '(UTC+01:00) Malta'),
('Europe/Monaco', '(UTC+01:00) Monaco'),
('Africa/Ndjamena', '(UTC+01:00) Ndjamena'),
('Africa/Niamey', '(UTC+01:00) Niamey'),
('Europe/Oslo', '(UTC+01:00) Oslo'),
('Europe/Paris', '(UTC+01:00) Paris'),
('Europe/Podgorica', '(UTC+01:00) Podgorica'),
('Africa/Porto-Novo', '(UTC+01:00) Porto-Novo'),
('Europe/Prague', '(UTC+01:00) Prague'),
('Europe/Rome', '(UTC+01:00) Rome'),
('Europe/San_Marino', '(UTC+01:00) San Marino'),
('Europe/Sarajevo', '(UTC+01:00) Sarajevo'),
('Europe/Skopje', '(UTC+01:00) Skopje'),
('Europe/Stockholm', '(UTC+01:00) Stockholm'),
('Europe/Tirane', '(UTC+01:00) Tirane'),
('Africa/Tripoli', '(UTC+01:00) Tripoli'),
('Africa/Tunis', '(UTC+01:00) Tunis'),
('Europe/Vaduz', '(UTC+01:00) Vaduz'),
('Europe/Vatican', '(UTC+01:00) Vatican'),
('Europe/Vienna', '(UTC+01:00) Vienna'),
('Europe/Warsaw', '(UTC+01:00) Warsaw'),
('Africa/Windhoek', '(UTC+01:00) Windhoek'),
('Europe/Zagreb', '(UTC+01:00) Zagreb'),
('Europe/Zurich', '(UTC+01:00) Zurich'),
('Europe/Athens', '(UTC+02:00) Athens'),
('Asia/Beirut', '(UTC+02:00) Beirut'),
('Africa/Blantyre', '(UTC+02:00) Blantyre'),
('Europe/Bucharest', '(UTC+02:00) Bucharest'),
('Africa/Bujumbura', '(UTC+02:00) Bujumbura'),
('Africa/Cairo', '(UTC+02:00) Cairo'),
('Europe/Chisinau', '(UTC+02:00) Chisinau'),
('Asia/Damascus', '(UTC+02:00) Damascus'),
('Africa/Gaborone', '(UTC+02:00) Gaborone'),
('Asia/Gaza', '(UTC+02:00) Gaza'),
('Africa/Harare', '(UTC+02:00) Harare'),
('Asia/Hebron', '(UTC+02:00) Hebron'),
('Europe/Helsinki', '(UTC+02:00) Helsinki'),
('Europe/Istanbul', '(UTC+02:00) Istanbul'),
('Asia/Jerusalem', '(UTC+02:00) Jerusalem'),
('Africa/Johannesburg', '(UTC+02:00) Johannesburg'),
('Europe/Kiev', '(UTC+02:00) Kiev'),
('Africa/Kigali', '(UTC+02:00) Kigali'),
('Africa/Lubumbashi', '(UTC+02:00) Lubumbashi'),
('Africa/Lusaka', '(UTC+02:00) Lusaka'),
('Africa/Maputo', '(UTC+02:00) Maputo'),
('Europe/Mariehamn', '(UTC+02:00) Mariehamn'),
('Africa/Maseru', '(UTC+02:00) Maseru'),
('Africa/Mbabane', '(UTC+02:00) Mbabane'),
('Asia/Nicosia', '(UTC+02:00) Nicosia'),
('Europe/Riga', '(UTC+02:00) Riga'),
('Europe/Simferopol', '(UTC+02:00) Simferopol'),
('Europe/Sofia', '(UTC+02:00) Sofia'),
('Europe/Tallinn', '(UTC+02:00) Tallinn'),
('Europe/Uzhgorod', '(UTC+02:00) Uzhgorod'),
('Europe/Vilnius', '(UTC+02:00) Vilnius'),
('Europe/Zaporozhye', '(UTC+02:00) Zaporozhye'),
('Africa/Addis_Ababa', '(UTC+03:00) Addis Ababa'),
('Asia/Aden', '(UTC+03:00) Aden'),
('Asia/Amman', '(UTC+03:00) Amman'),
('Indian/Antananarivo', '(UTC+03:00) Antananarivo'),
('Africa/Asmara', '(UTC+03:00) Asmara'),
('Asia/Baghdad', '(UTC+03:00) Baghdad'),
('Asia/Bahrain', '(UTC+03:00) Bahrain'),
('Indian/Comoro', '(UTC+03:00) Comoro'),
('Africa/Dar_es_Salaam', '(UTC+03:00) Dar es Salaam'),
('Africa/Djibouti', '(UTC+03:00) Djibouti'),
('Africa/Juba', '(UTC+03:00) Juba'),
('Europe/Kaliningrad', '(UTC+03:00) Kaliningrad'),
('Africa/Kampala', '(UTC+03:00) Kampala'),
('Africa/Khartoum', '(UTC+03:00) Khartoum'),
('Asia/Kuwait', '(UTC+03:00) Kuwait'),
('Indian/Mayotte', '(UTC+03:00) Mayotte'),
('Europe/Minsk', '(UTC+03:00) Minsk'),
('Africa/Mogadishu', '(UTC+03:00) Mogadishu'),
('Europe/Moscow', '(UTC+03:00) Moscow'),
('Africa/Nairobi', '(UTC+03:00) Nairobi'),
('Asia/Qatar', '(UTC+03:00) Qatar'),
('Asia/Riyadh', '(UTC+03:00) Riyadh'),
('Antarctica/Syowa', '(UTC+03:00) Syowa'),
('Asia/Tehran', '(UTC+03:30) Tehran'),
('Asia/Baku', '(UTC+04:00) Baku'),
('Asia/Dubai', '(UTC+04:00) Dubai'),
('Indian/Mahe', '(UTC+04:00) Mahe'),
('Indian/Mauritius', '(UTC+04:00) Mauritius'),
('Asia/Muscat', '(UTC+04:00) Muscat'),
('Indian/Reunion', '(UTC+04:00) Reunion'),
('Europe/Samara', '(UTC+04:00) Samara'),
('Asia/Tbilisi', '(UTC+04:00) Tbilisi'),
('Europe/Volgograd', '(UTC+04:00) Volgograd'),
('Asia/Yerevan', '(UTC+04:00) Yerevan'),
('Asia/Kabul', '(UTC+04:30) Kabul'),
('Asia/Aqtau', '(UTC+05:00) Aqtau'),
('Asia/Aqtobe', '(UTC+05:00) Aqtobe'),
('Asia/Ashgabat', '(UTC+05:00) Ashgabat'),
('Asia/Dushanbe', '(UTC+05:00) Dushanbe'),
('Asia/Karachi', '(UTC+05:00) Karachi'),
('Indian/Kerguelen', '(UTC+05:00) Kerguelen'),
('Indian/Maldives', '(UTC+05:00) Maldives'),
('Antarctica/Mawson', '(UTC+05:00) Mawson'),
('Asia/Oral', '(UTC+05:00) Oral'),
('Asia/Samarkand', '(UTC+05:00) Samarkand'),
('Asia/Tashkent', '(UTC+05:00) Tashkent'),
('Asia/Colombo', '(UTC+05:30) Colombo'),
('Asia/Kolkata', '(UTC+05:30) Kolkata'),
('Asia/Kathmandu', '(UTC+05:45) Kathmandu'),
('Asia/Almaty', '(UTC+06:00) Almaty'),
('Asia/Bishkek', '(UTC+06:00) Bishkek'),
('Indian/Chagos', '(UTC+06:00) Chagos'),
('Asia/Dhaka', '(UTC+06:00) Dhaka'),
('Asia/Qyzylorda', '(UTC+06:00) Qyzylorda'),
('Asia/Thimphu', '(UTC+06:00) Thimphu'),
('Antarctica/Vostok', '(UTC+06:00) Vostok'),
('Asia/Yekaterinburg', '(UTC+06:00) Yekaterinburg'),
('Indian/Cocos', '(UTC+06:30) Cocos'),
('Asia/Rangoon', '(UTC+06:30) Rangoon'),
('Asia/Bangkok', '(UTC+07:00) Bangkok'),
('Indian/Christmas', '(UTC+07:00) Christmas'),
('Antarctica/Davis', '(UTC+07:00) Davis'),
('Asia/Ho_Chi_Minh', '(UTC+07:00) Ho Chi Minh'),
('Asia/Hovd', '(UTC+07:00) Hovd'),
('Asia/Jakarta', '(UTC+07:00) Jakarta'),
('Asia/Novokuznetsk', '(UTC+07:00) Novokuznetsk'),
('Asia/Novosibirsk', '(UTC+07:00) Novosibirsk'),
('Asia/Omsk', '(UTC+07:00) Omsk'),
('Asia/Phnom_Penh', '(UTC+07:00) Phnom Penh'),
('Asia/Pontianak', '(UTC+07:00) Pontianak'),
('Asia/Vientiane', '(UTC+07:00) Vientiane'),
('Asia/Brunei', '(UTC+08:00) Brunei'),
('Antarctica/Casey', '(UTC+08:00) Casey'),
('Asia/Choibalsan', '(UTC+08:00) Choibalsan'),
('Asia/Chongqing', '(UTC+08:00) Chongqing'),
('Asia/Harbin', '(UTC+08:00) Harbin'),
('Asia/Hong_Kong', '(UTC+08:00) Hong Kong'),
('Asia/Kashgar', '(UTC+08:00) Kashgar'),
('Asia/Krasnoyarsk', '(UTC+08:00) Krasnoyarsk'),
('Asia/Kuala_Lumpur', '(UTC+08:00) Kuala Lumpur'),
('Asia/Kuching', '(UTC+08:00) Kuching'),
('Asia/Macau', '(UTC+08:00) Macau'),
('Asia/Makassar', '(UTC+08:00) Makassar'),
('Asia/Manila', '(UTC+08:00) Manila'),
('Australia/Perth', '(UTC+08:00) Perth'),
('Asia/Shanghai', '(UTC+08:00) Shanghai'),
('Asia/Singapore', '(UTC+08:00) Singapore'),
('Asia/Taipei', '(UTC+08:00) Taipei'),
('Asia/Ulaanbaatar', '(UTC+08:00) Ulaanbaatar'),
('Asia/Urumqi', '(UTC+08:00) Urumqi'),
('Australia/Eucla', '(UTC+08:45) Eucla'),
('Asia/Dili', '(UTC+09:00) Dili'),
('Asia/Irkutsk', '(UTC+09:00) Irkutsk'),
('Asia/Jayapura', '(UTC+09:00) Jayapura'),
('Pacific/Palau', '(UTC+09:00) Palau'),
('Asia/Pyongyang', '(UTC+09:00) Pyongyang'),
('Asia/Seoul', '(UTC+09:00) Seoul'),
('Asia/Tokyo', '(UTC+09:00) Tokyo'),
('Australia/Adelaide', '(UTC+09:30) Adelaide'),
('Australia/Broken_Hill', '(UTC+09:30) Broken Hill'),
('Australia/Darwin', '(UTC+09:30) Darwin'),
('Australia/Brisbane', '(UTC+10:00) Brisbane'),
('Pacific/Chuuk', '(UTC+10:00) Chuuk'),
('Australia/Currie', '(UTC+10:00) Currie'),
('Antarctica/DumontDUrville', '(UTC+10:00) DumontDUrville'),
('Pacific/Guam', '(UTC+10:00) Guam'),
('Australia/Hobart', '(UTC+10:00) Hobart'),
('Asia/Khandyga', '(UTC+10:00) Khandyga'),
('Australia/Lindeman', '(UTC+10:00) Lindeman'),
('Australia/Melbourne', '(UTC+10:00) Melbourne'),
('Pacific/Port_Moresby', '(UTC+10:00) Port Moresby'),
('Pacific/Saipan', '(UTC+10:00) Saipan'),
('Australia/Sydney', '(UTC+10:00) Sydney'),
('Asia/Yakutsk', '(UTC+10:00) Yakutsk'),
('Australia/Lord_Howe', '(UTC+10:30) Lord Howe'),
('Pacific/Efate', '(UTC+11:00) Efate'),
('Pacific/Guadalcanal', '(UTC+11:00) Guadalcanal'),
('Pacific/Kosrae', '(UTC+11:00) Kosrae'),
('Antarctica/Macquarie', '(UTC+11:00) Macquarie'),
('Pacific/Noumea', '(UTC+11:00) Noumea'),
('Pacific/Pohnpei', '(UTC+11:00) Pohnpei'),
('Asia/Sakhalin', '(UTC+11:00) Sakhalin'),
('Asia/Ust-Nera', '(UTC+11:00) Ust-Nera'),
('Asia/Vladivostok', '(UTC+11:00) Vladivostok'),
('Pacific/Norfolk', '(UTC+11:30) Norfolk'),
('Asia/Anadyr', '(UTC+12:00) Anadyr'),
('Pacific/Auckland', '(UTC+12:00) Auckland'),
('Pacific/Fiji', '(UTC+12:00) Fiji'),
('Pacific/Funafuti', '(UTC+12:00) Funafuti'),
('Asia/Kamchatka', '(UTC+12:00) Kamchatka'),
('Pacific/Kwajalein', '(UTC+12:00) Kwajalein'),
('Asia/Magadan', '(UTC+12:00) Magadan'),
('Pacific/Majuro', '(UTC+12:00) Majuro'),
('Antarctica/McMurdo', '(UTC+12:00) McMurdo'),
('Pacific/Nauru', '(UTC+12:00) Nauru'),
('Antarctica/South_Pole', '(UTC+12:00) South Pole'),
('Pacific/Tarawa', '(UTC+12:00) Tarawa'),
('Pacific/Wake', '(UTC+12:00) Wake'),
('Pacific/Wallis', '(UTC+12:00) Wallis'),
('Pacific/Chatham', '(UTC+12:45) Chatham'),
('Pacific/Apia', '(UTC+13:00) Apia'),
('Pacific/Enderbury', '(UTC+13:00) Enderbury'),
('Pacific/Fakaofo', '(UTC+13:00) Fakaofo'),
('Pacific/Tongatapu', '(UTC+13:00) Tongatapu'),
('Pacific/Kiritimati', '(UTC+14:00) Kiritimati');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('active','disabled') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'active',
  `timezone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'UTC',
  `code` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `language` enum('en','es') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'en'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `status`, `timezone`, `code`, `remember_token`, `email_verified_at`, `created_at`, `updated_at`, `deleted_at`, `language`) VALUES
(1, 'Guillermo Rolfson', 'admin@admin.com', '$2y$10$OIZeCJF6vQ.a4g5k93IGEeYhO6K9SFohlb2HWpMYHLlbHyOimWCyW', 'active', 'UTC', 'Vv8RKxqJ09', 'aI5XeDkXBV', '2021-07-04 11:53:23', '2021-07-04 11:53:23', '2021-07-04 11:53:23', NULL, 'en');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `accountsetting_staffs`
--
ALTER TABLE `accountsetting_staffs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `activity_log`
--
ALTER TABLE `activity_log`
  ADD PRIMARY KEY (`id`),
  ADD KEY `subject` (`subject_type`,`subject_id`),
  ADD KEY `causer` (`causer_type`,`causer_id`),
  ADD KEY `activity_log_log_name_index` (`log_name`);

--
-- Indexes for table `bookings`
--
ALTER TABLE `bookings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `booking_formvalues`
--
ALTER TABLE `booking_formvalues`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `brandcolors`
--
ALTER TABLE `brandcolors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `businesses`
--
ALTER TABLE `businesses`
  ADD PRIMARY KEY (`id`),
  ADD KEY `businesses_business_owner_id_foreign` (`business_owner_id`);

--
-- Indexes for table `business_locations`
--
ALTER TABLE `business_locations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `business_locations_business_id_foreign` (`business_id`);

--
-- Indexes for table `business_owners`
--
ALTER TABLE `business_owners`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `business_owners_code_unique` (`code`);

--
-- Indexes for table `business_plans`
--
ALTER TABLE `business_plans`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `business_plans_name_unique` (`name`),
  ADD UNIQUE KEY `business_plans_code_unique` (`code`);

--
-- Indexes for table `business_schedules`
--
ALTER TABLE `business_schedules`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `business_schedules_name_business_id_unique` (`name`,`business_id`),
  ADD KEY `business_schedules_business_id_foreign` (`business_id`);

--
-- Indexes for table `business_services`
--
ALTER TABLE `business_services`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `business_services_code_unique` (`code`),
  ADD KEY `business_services_business_id_foreign` (`business_id`),
  ADD KEY `business_services_signup_form_id_foreign` (`signup_form_id`),
  ADD KEY `business_services_service_category_id_foreign` (`service_category_id`);

--
-- Indexes for table `business_service_categories`
--
ALTER TABLE `business_service_categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `business_service_categories_business_id_foreign` (`business_id`);

--
-- Indexes for table `business_service_signup_forms`
--
ALTER TABLE `business_service_signup_forms`
  ADD PRIMARY KEY (`id`),
  ADD KEY `business_service_signup_forms_business_id_foreign` (`business_id`);

--
-- Indexes for table `business_service_signup_form_fields`
--
ALTER TABLE `business_service_signup_form_fields`
  ADD PRIMARY KEY (`id`),
  ADD KEY `business_service_signup_form_fields_signup_form_id_foreign` (`signup_form_id`);

--
-- Indexes for table `business_service_staff_members`
--
ALTER TABLE `business_service_staff_members`
  ADD PRIMARY KEY (`id`),
  ADD KEY `business_service_staff_members_business_service_id_foreign` (`business_service_id`),
  ADD KEY `business_service_staff_members_business_staff_member_id_foreign` (`business_staff_member_id`);

--
-- Indexes for table `business_staff_members`
--
ALTER TABLE `business_staff_members`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `business_staff_members_code_unique` (`code`),
  ADD UNIQUE KEY `business_staff_members_g_selected_email_unique` (`g_selected_email`),
  ADD UNIQUE KEY `business_staff_members_g_calendar_id_unique` (`g_calendar_id`),
  ADD KEY `business_staff_members_business_id_foreign` (`business_id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `g_calendars`
--
ALTER TABLE `g_calendars`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `g_calendars_account_unique` (`account`),
  ADD KEY `g_calendars_staff_id_foreign` (`staff_id`);

--
-- Indexes for table `jobs`
--
ALTER TABLE `jobs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `jobs_queue_index` (`queue`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`model_id`,`model_type`),
  ADD KEY `model_has_permissions_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indexes for table `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD PRIMARY KEY (`role_id`,`model_id`,`model_type`),
  ADD KEY `model_has_roles_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `permissions_name_guard_name_unique` (`name`,`guard_name`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `role_has_permissions_role_id_foreign` (`role_id`);

--
-- Indexes for table `sidebarlogos`
--
ALTER TABLE `sidebarlogos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `side_barcolor_businesses`
--
ALTER TABLE `side_barcolor_businesses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `side_logo_businesses`
--
ALTER TABLE `side_logo_businesses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `staffservices`
--
ALTER TABLE `staffservices`
  ADD PRIMARY KEY (`id`),
  ADD KEY `staffservices_staff_id_foreign` (`staff_id`);

--
-- Indexes for table `staff_booking_formfields`
--
ALTER TABLE `staff_booking_formfields`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `staff_schedules`
--
ALTER TABLE `staff_schedules`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `staff_schedules_name_staff_id_unique` (`name`,`staff_id`),
  ADD KEY `staff_schedules_staff_id_foreign` (`staff_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_code_unique` (`code`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `accountsetting_staffs`
--
ALTER TABLE `accountsetting_staffs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `activity_log`
--
ALTER TABLE `activity_log`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `bookings`
--
ALTER TABLE `bookings`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `booking_formvalues`
--
ALTER TABLE `booking_formvalues`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `brandcolors`
--
ALTER TABLE `brandcolors`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `businesses`
--
ALTER TABLE `businesses`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `business_locations`
--
ALTER TABLE `business_locations`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `business_owners`
--
ALTER TABLE `business_owners`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `business_plans`
--
ALTER TABLE `business_plans`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `business_schedules`
--
ALTER TABLE `business_schedules`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `business_services`
--
ALTER TABLE `business_services`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `business_service_categories`
--
ALTER TABLE `business_service_categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `business_service_signup_forms`
--
ALTER TABLE `business_service_signup_forms`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `business_service_signup_form_fields`
--
ALTER TABLE `business_service_signup_form_fields`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `business_service_staff_members`
--
ALTER TABLE `business_service_staff_members`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `business_staff_members`
--
ALTER TABLE `business_staff_members`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `g_calendars`
--
ALTER TABLE `g_calendars`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `jobs`
--
ALTER TABLE `jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `sidebarlogos`
--
ALTER TABLE `sidebarlogos`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `side_barcolor_businesses`
--
ALTER TABLE `side_barcolor_businesses`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `side_logo_businesses`
--
ALTER TABLE `side_logo_businesses`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `staffservices`
--
ALTER TABLE `staffservices`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `staff_booking_formfields`
--
ALTER TABLE `staff_booking_formfields`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;

--
-- AUTO_INCREMENT for table `staff_schedules`
--
ALTER TABLE `staff_schedules`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `businesses`
--
ALTER TABLE `businesses`
  ADD CONSTRAINT `businesses_business_owner_id_foreign` FOREIGN KEY (`business_owner_id`) REFERENCES `business_owners` (`id`);

--
-- Constraints for table `business_locations`
--
ALTER TABLE `business_locations`
  ADD CONSTRAINT `business_locations_business_id_foreign` FOREIGN KEY (`business_id`) REFERENCES `businesses` (`id`);

--
-- Constraints for table `business_schedules`
--
ALTER TABLE `business_schedules`
  ADD CONSTRAINT `business_schedules_business_id_foreign` FOREIGN KEY (`business_id`) REFERENCES `businesses` (`id`);

--
-- Constraints for table `business_services`
--
ALTER TABLE `business_services`
  ADD CONSTRAINT `business_services_business_id_foreign` FOREIGN KEY (`business_id`) REFERENCES `businesses` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `business_services_service_category_id_foreign` FOREIGN KEY (`service_category_id`) REFERENCES `business_service_categories` (`id`),
  ADD CONSTRAINT `business_services_signup_form_id_foreign` FOREIGN KEY (`signup_form_id`) REFERENCES `business_service_signup_forms` (`id`);

--
-- Constraints for table `business_service_categories`
--
ALTER TABLE `business_service_categories`
  ADD CONSTRAINT `business_service_categories_business_id_foreign` FOREIGN KEY (`business_id`) REFERENCES `businesses` (`id`);

--
-- Constraints for table `business_service_signup_forms`
--
ALTER TABLE `business_service_signup_forms`
  ADD CONSTRAINT `business_service_signup_forms_business_id_foreign` FOREIGN KEY (`business_id`) REFERENCES `businesses` (`id`);

--
-- Constraints for table `business_service_signup_form_fields`
--
ALTER TABLE `business_service_signup_form_fields`
  ADD CONSTRAINT `business_service_signup_form_fields_signup_form_id_foreign` FOREIGN KEY (`signup_form_id`) REFERENCES `business_service_signup_forms` (`id`);

--
-- Constraints for table `business_service_staff_members`
--
ALTER TABLE `business_service_staff_members`
  ADD CONSTRAINT `business_service_staff_members_business_service_id_foreign` FOREIGN KEY (`business_service_id`) REFERENCES `business_services` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `business_service_staff_members_business_staff_member_id_foreign` FOREIGN KEY (`business_staff_member_id`) REFERENCES `business_staff_members` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `business_staff_members`
--
ALTER TABLE `business_staff_members`
  ADD CONSTRAINT `business_staff_members_business_id_foreign` FOREIGN KEY (`business_id`) REFERENCES `businesses` (`id`);

--
-- Constraints for table `g_calendars`
--
ALTER TABLE `g_calendars`
  ADD CONSTRAINT `g_calendars_staff_id_foreign` FOREIGN KEY (`staff_id`) REFERENCES `business_staff_members` (`id`);

--
-- Constraints for table `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD CONSTRAINT `model_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD CONSTRAINT `model_has_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD CONSTRAINT `role_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_has_permissions_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `staffservices`
--
ALTER TABLE `staffservices`
  ADD CONSTRAINT `staffservices_staff_id_foreign` FOREIGN KEY (`staff_id`) REFERENCES `business_staff_members` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `staff_schedules`
--
ALTER TABLE `staff_schedules`
  ADD CONSTRAINT `staff_schedules_staff_id_foreign` FOREIGN KEY (`staff_id`) REFERENCES `business_staff_members` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
