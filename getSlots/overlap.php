<?php

if(isset($bookingsOFTHISDAY)){
for($i = 0; $i < count($bookingsOFTHISDAY); $i++){
    $bs = $bookingsOFTHISDAY[$i]['startTime'];
    $be = $bookingsOFTHISDAY[$i]['endTime'];

    $bs->settimezone($userTimezone);
    $be->settimezone($userTimezone);

    if($as->format("H:i:s") >= $bs->format("H:i:s") && $ae->format("H:i:s") <= $be->format("H:i:s")){
        $block = "YES";
    } else if($as->format("H:i:s") < $bs->format("H:i:s") && $ae->format("H:i:s") >= $bs->format("H:i:s") && $ae->format("H:i:s") <= $be->format("H:i:s")){
        $block = "PARTIAL";

        array_push($availableTimeArray['startTime'], $as->format("Y-m-d H:i:s"));  
        array_push($availableTimeArray['endTime'], $bs->format("Y-m-d H:i:s"));  

    } else if($be->format("H:i:s") < $ae->format("H:i:s") && $as->format("H:i:s") >= $bs->format("H:i:s") && $as->format("H:i:s") <= $be->format("H:i:s")){
        $block = "PARTIAL";

        array_push($availableTimeArray['startTime'], $be->format("Y-m-d H:i:s"));
        array_push($availableTimeArray['endTime'], $ae->format("Y-m-d H:i:s"));
    } else if($as->format("H:i:s") <= $bs->format("H:i:s") && $ae->format("H:i:s") >= $be->format("H:i:s")){
        $block = "PARTIAL";

        array_push($availableTimeArray['startTime'], $as->format("Y-m-d H:i:s"));
        array_push($availableTimeArray['endTime'], $bs->format("Y-m-d H:i:s"));

        array_push($availableTimeArray['startTime'], $be->format("Y-m-d H:i:s"));
        array_push($availableTimeArray['endTime'], $ae->format("Y-m-d H:i:s"));

    }
}
}

?>